from threading import Thread, Event
from Queue import Queue, Empty
import traceback

class ThreadIface(Thread, object):
    def __init__(self, _in = None, _out = None, _res = None, _stop = Event()):
        Thread.__init__(self)
        print("iface ctor")
        self.__inputQueue = _in
        self.__outputQueue = _out
        self.__resultQueue = _res
        self.__event = _stop
        self.daemon = True

    def task(self):
        pass

    def run(self):
        while not self.__event.isSet():
            self.task()

class SearchThread(ThreadIface):
    def __init__(self, _in = None, _out = None, _stop = Event()):
        ThreadIface.__init__(self, _in = _in, _out = _out, _stop = _stop)

    def task(self):
        while True:
            try:
                element = self.__inputQueue.get()
                print(element)
            except Empty:
                print("Empty!!!!")
            except Exception:
                print(traceback.format_exc())
                break

inputQueue = Queue()
outputQueue = Queue()
stop = Event()

for i in range(10):
    inputQueue.put(i)

t = SearchThread(_in = inputQueue, _out = outputQueue, _stop = stop)
t.start()
t.join()

class A(object):
    def __init__(self, p1 = 5):
        print("A ctor")
        self._a = p1

    def m(self):
        print("A:Method --> ", self._a)

class B(A):
    def __init__(self, p1 = 10, p2 = 20):
        A.__init__(self, p1)
        self._b = p2
        print("B ctor")

    def m(self):
        print("B:method --> ", self._a, self._b)
#
# a = A(5)
# a.m()
#
# b = B(15)
# b.m()