# -*- coding: utf-8 -*-
__author__ = 'sony'

from bs4 import BeautifulSoup
from copy import deepcopy, copy
import urllib2
import time


import useragent


PARSER_HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "User-Agent": ""
}

class WebPage(object):
    def __init__(self, content):
        self.__content = content
        self.__parsedPage = BeautifulSoup(self.__content)

    def get(self, tags=list(), all=False):
        result = self.__parsedPage.find(*tags) if not all else self.__parsedPage.find_all(*tags)
        return result

    def setExtaData(self, extraData=dict()):
        self.__metaData = deepcopy(extraData)

    def save(self, path=None):
        pass

class PageOpener(object):
    def __init__(self):
        global PARSER_HEADERS
        self.__headers = deepcopy(PARSER_HEADERS)
        self.__headers["User-Agent"] = useragent.getRandomUserAgent()

    def _modHeader(self, responce, urllink):
        global PARSER_HEADERS
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        if "Cookie" not in self.__headers.keys() or not self.__headers["Cookie"]:
            self.__headers["Cookie"] = cookie

    def open(self, url_to_visit):
        # try:
        # print("Opening page %s" % url_to_visit)
        request = urllib2.Request(url_to_visit, None, self.__headers)
        t = time.time()
        responce = urllib2.urlopen(request, timeout=20)
        # print("Estimated time for page load is: %d" % (time.time() - t))
        self._modHeader(responce, url_to_visit)
        return responce.read()
        # except urllib2.HTTPError, e:
        # print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
        #     sysutils.sysReconnect()
        #     raise Exception
        # except:
        #     print("Exception on search page %s. Details: %s" % (url_to_visit, traceback.format_exc()))
        #     # sysutils.sysReconnect()
        #     # raise Exception