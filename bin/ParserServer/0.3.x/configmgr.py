# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import json, settings

#############################################
# Config anger section                      #
#############################################

class ConfigManager(object):
    def __init__(self):
        self.__moduleConfigs = dict()
        self.load()

    def load(self):
        config = json.load(open(settings.CONFIG_LOCATION, "r"))
        for mod_name, mod_val in config.items:
            m_cfg = json.load(open(mod_val["path"], "r"))
            self.__moduleConfigs[mod_name] = m_cfg

    def getConfig(self, moduleName):
        if moduleName in self.__moduleConfigs.keys():
            return self.__moduleConfigs[moduleName]

    def setConfig(self):
        pass

#############################################
# Configuration unit section                #
#############################################

class ConfigUnit(object):
    def __init__(self):
        self.__data = dict()

    def setParam(self, param, value):
        self.__data[param] = value

    def getParam(self, param):
        if param in self.__data.keys():
            return self.__data[param]