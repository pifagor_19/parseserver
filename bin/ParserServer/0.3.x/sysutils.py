# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import os, settings, time
from threading import RLock

global_lock = RLock()
lock_time = 0

def sysReconnect():
    global lock_time, global_lock
    provider = settings.MODEM_PROVIDER
    if time.time() - lock_time > settings.LOCK_DELTA_TIME:
        print("Last lock time is: %d" % lock_time)
        lock_time = time.time()
        with global_lock:
            print("LOCK THREADS")
            for command in settings.RECONNECT_SYS[provider]:
                _executeCommand(**command)
            # _executeCommand(**settings.RECONNECT_SYS["enable"])

def powerOnIface():
    print("Power on network interface ...")
    _executeCommand(**settings.RECONNECT_SYS["enable"])
    print("[OK] Network interface is switched on.")

def _executeCommand(**commands):
    print("Executing command %s" % commands["command"])
    os.system(commands["command"])
    if "timeout" in commands:
        time.sleep(commands["timeout"])

class ModemSwitcher(object):
    def __init__(self):
        self.__provider = str()

    def reloadModem(self):
        pass

if __name__ == "__main__":
    print("Test")
    sysReconnect()