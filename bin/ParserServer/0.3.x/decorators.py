# -*- coding: utf-8 -*-
__author__ = 'sony'

import traceback
import urllib2

import sysutils

def ExcHandler(func):
    def insideFunction(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result
        except Exception as e:
            print(traceback.format_exc())
    return insideFunction

def FatalExcHandler(func):
    def insideFunction(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result
        except urllib2.HTTPError, e:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            sysutils.sysReconnect()
    return insideFunction