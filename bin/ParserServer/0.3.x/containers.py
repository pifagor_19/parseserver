__author__ = 'sony'

import json

class DataContainer(object):
    def __init__(self):
        self.__fields = list()
        self.__data = dict()

    def setParam(self, field, value):
        if not field: return
        self.__data[field] = value

    def getParam(self, param):
        if param in self.__data:
            return self.__data[param]

    def getFields(self):
        return self.__data.keys()

    def asText(self):
        return json.dumps(self.__data, ensure_ascii=False)
