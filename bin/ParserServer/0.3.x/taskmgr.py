# -*- coding: utf-8 -*-
__author__ = 'sony'

from threading import Thread
from copy import deepcopy
from datetime import datetime, timedelta
from Queue import Queue, Empty
import json
import traceback
import urllib2

from webutils import PageOpener, WebPage
from kernel import DataContainer
from webparser import RULES_SYSTEM
import sysutils
import settings
import webparser


def get_date(datestamp):
    datestamp = datestamp.lower()
    today = datetime.now()
    rep_d = {
        "сегодня": today.strftime("%d %b"),
        "вчера": (today - timedelta(days=1)).strftime("%d %b"),
        "авг.": "Aug",
        "сен.": "Sep",
        "окт.": "Oct",
        "размещено ": "",
        "в ": ""
    }
    result = datestamp.encode("utf-8")
    for k, v in rep_d.items():
        result = result.replace(k, v)
    d = today.strftime("%Y") + ' ' + result
    return datetime.strptime(d, "%Y %d %b %H:%M")

class ResultThreadParser(Thread):
    def __init__(self, resultQueue, stopEvt, result):
        global RULES_SYSTEM
        Thread.__init__(self)
        self.__resultQueue = resultQueue
        self.__stopEvent = stopEvt
        self.__opener = PageOpener()
        self.daemon = True
        self.__config = deepcopy(RULES_SYSTEM)
        self.__result = result

    def __del__(self):
        print("Deleting thread")

    def get_url(self, url, fromUrl):
        # time.sleep(random.randint(0,5))
        base_url = "https://m.avito.ru"
        url_to_visit = base_url + url.get("href") + "?async"
        print("Check system: %d" % url_to_visit.find(fromUrl))
        self.__opener._modHeader(None, fromUrl)
        _result = self.__opener.open(url_to_visit)
        print(_result)
        obj = json.loads(_result)
        print(obj["phone"])
        return obj["phone"]

    def task(self):
        try:
            search_page = self.__resultQueue.get()
            print("Opening result page %s" % search_page)
            content = self.__opener.open(search_page)
            # print(content)
            webPage = WebPage(content)
            if self.__config:
                advert = DataContainer()
                for rname, rule in self.__config["content"].items:
                    tags = webPage.get(tags=rule["tags"])
                    result = eval(rule["rule"])(tags)
                    advert.setParam(rname, result)
                advert.setParam("url", search_page)
                # t1 = time.time()
                phone = self.get_url(
                    webPage.get(tags=["a", "person-action button button-blue action-show-number action-link link "]),
                    search_page)
                # print("Estimated time for parse phone page is: %d" % (time.time() - t1))
                advert.setParam("phone", phone.replace('tel:', '').replace('-', '').replace(' ', ''))
                datestamp = get_date(webPage.get(tags=["div", "item-add-date"]).text)
                advert.setParam("datestamp", datestamp)
                self.__result.put(advert)

            else:
                print("Empty config. Do nothing.")
        except Empty:
            pass
        except urllib2.HTTPError as e:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            self.__resultQueue.put(search_page)
            sysutils.sysReconnect()
        except AttributeError:
            print("Attribute Error!!!")
        except Exception:
            print(traceback.format_exc())
            self.__resultQueue.put(search_page)

        self.__resultQueue.task_done()

    def run(self):
        while not self.__stopEvent.isSet():
            self.task()


class SearchThreadParser(Thread):
    def __init__(self, searchQueue, resulQueue, stopEvt):
        Thread.__init__(self)
        self.__searchQueue = searchQueue
        self.__resultQueue = resulQueue
        self.__stopEvent = stopEvt
        self.__opener = PageOpener()
        self.daemon = True

    def __del__(self):
        print("Deleting thread")

    def task(self):
        global base_url, EXISTING_ADVERTS
        try:
            search_page = self.__searchQueue.get()
            content = self.__opener.open(search_page)
            webPage = WebPage(content)
            adverts = webPage.get(all=True, tags=["article"])
            print("Adverts amount: %d" % len(adverts))
            for ad in adverts:
                _url = base_url + ad.a.get("href")
                _date = ad.find("div", "info-date info-text").text.replace("\n", "").replace(",", "").replace(" ", "")
                print(_url)
                # print(get_date(_date))
                if _url not in EXISTING_ADVERTS:
                    self.__resultQueue.put(_url)
                    EXISTING_ADVERTS.append(_url)

        except Empty:
            pass
        except urllib2.HTTPError:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            self.__searchQueue.put(search_page)
            sysutils.sysReconnect()
        except Exception:
            print(traceback.format_exc())
            self.__searchQueue.put(search_page)
        self.__searchQueue.task_done()

    def run(self):
        while not self.__stopEvent.isSet():
            self.task()


# -----------------------------------------------------------

class ThreadManager(object):
    def __init__(self, stopEvent):
        self.__seachQueue = Queue()
        self.__resultQueue = Queue()
        self.__returnQueue = Queue()
        self.__searchPool = list()
        self.__resultPool = list()
        self.__event = stopEvent
        self._manage()

    def _manage(self):
        self._create()
        self._start()

    def _create(self):
        for _ in xrange(settings.SEARCH_THREADS_COUNT):
            _st = SearchThreadParser(self.__seachQueue, self.__resultQueue, self.__event)
            self.__searchPool.append(_st)
        for _ in xrange(settings.PARSE_THREADS_COUNT):
            _rt = ResultThreadParser(self.__resultQueue, self.__event, self.__returnQueue)
            self.__resultPool.append(_rt)

    def _start(self):
        for _st in self.__searchPool:
            _st.start()
        for _rt in self.__resultPool:
            _rt.start()

    def put_task(self, task_item):
        self.__seachQueue.put(task_item)

    def _retrieveResult(self):
        result = list()
        while True:
            try:
                result.append(self.__returnQueue.get_nowait())
            except Empty:
                break

        return result

    def wait_lock(self):
        self.__seachQueue.join()
        self.__resultQueue.join()
        result = self._retrieveResult()
        return result