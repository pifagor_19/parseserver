# -*- coding: utf-8 -*-

__author__ = 'adm1n'

from datetime import datetime, timedelta
from Queue import Queue
from threading import Event

import settings
from dbmgr import DBManager
from configmgr import ConfigManager
from scheduler import Scheduler
from containers import DataContainer
from taskmgr import ThreadManager

# -----------------------------------------------------------

base_url = "https://m.avito.ru"
RULES_SYSTEM = {}
EXISTING_ADVERTS = list()  # dict()
table_name = "adverts_moscow"
# -----------------------------------------------------------

class ParserQueue(Queue):
    def _init(self, maxsize):
        Queue._init(self, maxsize)
        self.all_items = list()

    def _put(self, item):
        if item not in self.all_items:
            Queue._put(self, item)
            self.all_items.append(item)  # add(item)

    def _get(self):
        Queue._get(self)
        return self.all_items.pop()


# -----------------------------------------------------------

class UrlBuilder(object):
    def __init__(self, town):
        self.__baseurl = "https://m.avito.ru"
        self.__location = town  # "lyubertsy"
        self.__category = "kvartiry"
        self.__adtype = "sdam"
        self.__sortfilter = "user=1&page=%d"
        self.__url = self.__baseurl + "/" + \
                     self.__location + "/" + \
                     self.__category + "/" + \
                     self.__adtype + "?" + \
                     self.__sortfilter

    def build(self, loc, cat, adtype, filter):
        pass

    def getUrl(self):
        return self.__url

    def getBaseUrl(self):
        return self.__baseurl


# -----------------------------------------------------------



# -----------------------------------------------------------

class ItemCollector(DataContainer):
    def __init__(self):
        DataContainer.__init__(self)




# -----------------------------------------------------------

class RuleSystem(object):
    def __init__(self):
        self.__rules = dict()

    def load(self, **kwargs):
        for k, v in kwargs.items():
            self.__rules[k] = v

    def get(self):
        pass


# -----------------------------------------------------------

class WebParser(object):
    def __init__(self, dbManager):
        self.__stopEvent = Event()
        self.__threadManager = ThreadManager(self.__stopEvent)
        self.__dbManager = dbManager

    def _events(self):
        return {"update": self.put_task}

    def __del__(self):
        self.__stopEvent.set()

    def load_db(self):
        global EXISTING_ADVERTS, table_name

        sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) BETWEEN '%s' AND '%s' ORDER BY guid;" \
                     % ("guid", table_name, (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d"),
                        datetime.now().strftime("%Y-%m-%d"))
        #sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) = '%s' ORDER BY guid;" \
        #                     % (", ".join(self.__fields), table_name, datetime.now().strftime("%Y-%m-%d"))
        data = self.__dbManager.fetch(sql_string)
        for row in data:
            guid = row[0]
            if guid not in EXISTING_ADVERTS:
                EXISTING_ADVERTS.append(row[0])

    def save_db(self, table_name, result):
        success = 0
        failure = 0
        for item in result:
            fields = item.getFields()
            values = [item.getParam(field) for field in fields]
            try:
                sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                    table=table_name,
                    keys=",".join(fields),
                    values=", ".join(["%s" for i in fields])
                )
                self.__dbManager.fetch(sql_string, values, returnData=False)
                success += 1
            except:
                print("Unable to add this ad - already exists")
                failure += 1
        print("Data import to DB completed. Inserted items: %d. Failed items: %d" % (success, failure))

    def put_task(self):
        global table_name
        town = "moskva"
        page_template = 'https://m.avito.ru/{0}/kvartiry/sdam?user=1&page=%d'.format(town)
        for page_no in xrange(settings.START_PAGE, settings.MAX_PAGE_COUNT):
            self.__threadManager.put_task(page_template % page_no)
        result = self.__threadManager.wait_lock()
        print("<----------- Got result ------------>")
        print("Total amount of items: %d" % len(result))
        self.save_db(table_name, result)

    def put_task(self, page):
        global table_name
        town = "moskva"
        page_template = 'https://m.avito.ru/{0}/kvartiry/sdam?user=1&page=%d'.format(town)
        for page_no in xrange(settings.START_PAGE, settings.MAX_PAGE_COUNT):
            self.__threadManager.put_task(page_template % page_no)
        result = self.__threadManager.wait_lock()
        print("<----------- Got result ------------>")
        print("Total amount of items: %d" % len(result))
        self.save_db(table_name, result)


# -----------------------------------------------------------

class Task(DataContainer):
    def __init__(self):
        pass


# -----------------------------------------------------------

class TaskManager(object):
    def __init__(self):
        pass

    def createTask(self):
        pass

    def removeTask(self):
        pass

    def registerTask(self):
        pass

    def unregisterTask(self):
        pass

    def getTask(self):
        pass

def main():
    global RULES_SYSTEM
    config = ConfigManager()
    dbmanager = DBManager(driver="PG")
    dbmanager.setCreds("host=127.0.0.1 dbname=avitoparser user=postgres password=acuario")
    RULES_SYSTEM = config.getConfig("WebCrawler")
    # rs = RuleSystem()
    # rs.load(**config.getConfig("WebCrawler"))
    evt = Event()
    parser = WebParser(dbmanager)
    # parser.load_db()
    # parser.setRules(**config.getConfig("WebCrawler"))
    shed = Scheduler(parser, evt)
    shed.start()
    # parser.put_task()


# if __name__ == '__main__':
#     main()
