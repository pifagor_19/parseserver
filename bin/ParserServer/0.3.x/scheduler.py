# -*- coding: utf-8 -*-
__author__ = 'sony'

from threading import Thread
from datetime import datetime, timedelta
import time
import thread

class Scheduler(Thread):
    def __init__(self, kernel, event):
        Thread.__init__(self)
        self.__stopEvent = event
        self.__kernel = kernel
        today = datetime.now()
        self.__schedList = {
            "update": today + timedelta(minutes=0)
        }

    def registerTask(self):
        pass

    def unregisterTask(self):
        pass

    def run(self):
        while not self.__stopEvent.wait(5):
            today = datetime.now()
            events = self.__kernel._events()
            for action, stamp in self.__schedList.items():
                if today > stamp:
                    t = time.time()
                    print("Action: %s. Datetime: %s.Now is: %s" % (action, stamp, today))
                    print(self.__schedList)
                    self.__schedList[action] += timedelta(seconds=20)
                    thread.start_new_thread(events[action], ())
                    # events[action]()
                    print("Estimated time to execute task is %s" % str(time.time() - t))
                    print("-------------------- >>>>>>>>>>>>>>>> Schedule completed")