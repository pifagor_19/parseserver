# -*- coding: utf-8 -*-
__author__ = 'sony'

from threading import Event
import json

from configmgr import ConfigManager
from dbmgr import DBManager
from webparser import WebParser, RULES_SYSTEM
from scheduler import Scheduler


class Kernel(object):
    def __init__(self):
        global RULES_SYSTEM
        self.__cfgManager = ConfigManager()
        self.__dbManager = DBManager(driver="PG")
        self.__dbManager.setCreds(' '.join([k+'='+v for k,v in self.__cfgManager.getConfig('DBManager')]))
        self._webParser = WebParser(self.__dbManager)
        RULES_SYSTEM = self.__cfgManager.getConfig('WebParser')
        self.__stopEvent = Event()
        self.__scheduler = Scheduler(self._webParser, self.__stopEvent)
        self.__scheduler.start()
    #
    #
    #
    #
    #
    #
    #
    #
    #     global RULES_SYSTEM
    # config = ConfigManager()
    # dbmanager = DBManager(driver="PG")
    # dbmanager.setCreds("host=127.0.0.1 dbname=avitoparser user=postgres password=acuario")
    # RULES_SYSTEM = config.getConfig("WebCrawler")
    # # rs = RuleSystem()
    # # rs.load(**config.getConfig("WebCrawler"))
    # evt = Event()
    # parser = WebParser(dbmanager)
    # # parser.load_db()
    # # parser.setRules(**config.getConfig("WebCrawler"))
    # shed = Scheduler(parser, evt)
    # shed.start()
    # # parser.put_task()
