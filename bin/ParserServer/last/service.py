# -*- coding: utf-8 -*-
__author__ = 'sony'

import logger
import logging
import traceback
from kernel import Kernel


def main():
    try:
        kernel = Kernel()
        kernel.load()
        kernel.test()
    except:
        logging.critical("An exception occurred. Details: %s" % traceback.format_exc())

if __name__ == '__main__':
    main()