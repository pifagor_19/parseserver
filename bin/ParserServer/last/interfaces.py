__author__ = 'sony'

from container import DataContainer


class BaseModule(object):
    required_fields = []
    additional_fields = []

    def __init__(self):
        self.__settings = DataContainer()
        self.__data = None

    def load(self):
        pass

    @property
    def required(self):
        return self.required_fields

    @required.setter
    def set_required(self):
        raise AttributeError('Setting module required parameter is prohibited')

    @property
    def additional(self):
        return self.__additional_fields

    @additional.setter
    def set_additional(self):
        raise AttributeError('Setting module additional parameter is prohibited')

    def unload(self):
        pass

    def set_settings(self, settings):
        if settings:
            for key, value in settings.items():
                self.__settings.set(key, value)

    def get_settings(self, param, def_value):
        if param:
            if param in self.__settings.fields:
                return self.__settings.get(param, def_value)
        else:
            return self.__settings