# -*- coding: utf-8 -*-
__author__ = 'sony'

from copy import deepcopy
from bs4 import BeautifulSoup
import urllib2
import logging

import useragent
import settings
import decorators


class WebPage(object):
    def __init__(self, content):
        self.__content = content
        self.__parsedPage = BeautifulSoup(self.__content)

    def get(self, tags=list(), all_data=False):
        result = self.__parsedPage.find(*tags) if not all_data else self.__parsedPage.find_all(*tags)
        return result


class PageOpener(object):
    def __init__(self):
        self.__headers = deepcopy(settings.PARSER_HEADERS)
        self.__headers['User-Agent'] = useragent.getRandomUserAgent()

    def _mod_header(self, response, url_link):
        self.__headers['Referer'] = url_link
        cookie = ''
        cookie_list = ['sessid=', 'u=', 'v=']
        if response:
            for it in response.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace('Set-Cookie: ', '').split(';')[0] + ';'
        if 'Cookie' not in self.__headers.keys() \
                or not self.__headers['Cookie']:
            self.__headers['Cookie'] = cookie

    @decorators.profiler
    def open(self, url_to_visit):
        request = urllib2.Request(url_to_visit, None, self.__headers)
        response = urllib2.urlopen(request, timeout=20)
        self._mod_header(response, url_to_visit)
        return response.read()