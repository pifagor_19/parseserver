# -*- coding: utf-8 -*-
__author__ = 'sony'

import time
import thread
import logging
import traceback
from threading import Thread
from datetime import datetime, timedelta

from interfaces import BaseModule
from container import DataContainer
from executor import TaskExecutor
import lambdas

__priority_type = dict(low=0, normal=1, high=2)
__execution_type = dict(sync=0, async=1)


class SchedulerItem(object):
    def __init__(self, task_guid, task_name, scheduler_opts, exec_type='sync'):
        self.__guid = task_guid
        self.__name = task_name
        self.__execution_type = exec_type

        dt_now = datetime.now()

        start_time = scheduler_opts['start'].split('.')
        self.__start_time = datetime(year=dt_now.year, month=dt_now.month,
                                     day=dt_now.day, hour=int(start_time[0]),
                                     minute=int(start_time[1]))

        stop_time = scheduler_opts['stop'].split('.')
        self.__stop_time = datetime(year=dt_now.year, month=dt_now.month,
                                     day=dt_now.day, hour=int(stop_time[0]),
                                     minute=int(stop_time[1]))

        interval = scheduler_opts['interval'].split('.')
        self.__interval = [int(i) for i in interval]
        self.__priority = scheduler_opts['priority']
        self.__day_range = [int(i) for i in scheduler_opts['days']]
        self.__last_execute = self.__start_time

    def update_schedule(self):
        self.__last_execute = datetime.now() + timedelta(hours=self.__interval[0],
                                                         minutes=self.__interval[1],
                                                         seconds=self.__interval[2])

    def can_start(self):
        tick = datetime.now()

        def check_time():
            return self.__last_execute < tick

        def check_time_interval():
            return self.__start_time < tick < self.__stop_time

        def check_date_interval():
            return tick.weekday() in self.__day_range

        return check_time() and check_time_interval() and check_date_interval()


class Scheduler(Thread, BaseModule):
    required_fields = []
    additional_fields = []

    def __init__(self, kernel, event):
        Thread.__init__(self)
        BaseModule.__init__(self)
        self.__stopEvent = event
        self.__taskManager = kernel
        self.__executor = TaskExecutor()
        self.__schedule = {}

    def reg_all_tasks(self):
        for guid, task in self.__taskManager.tasks().container.items():
            if guid not in self.__schedule.keys():
                self.reg_task(guid, task)
            else:
                pass

    def unreg_all(self):
        pass

    def reg_task(self, guid, task):
        logging.debug('Registering task %s of type %s' % (guid, lambdas.module_name(task)))
        is_runnable = task.get_param('runnable')
        if not is_runnable:
            logging.info('Task %s is not runnable, skipping...' % guid)
            return
        name = task.get_param('name')
        scheduling = task.get_param('schedule')
        exec_type = task.get_param('exec_type')
        schedule_item = SchedulerItem(guid, name, scheduling, exec_type)
        self.__schedule[guid] = schedule_item

    def unreg_task(self, guid):
        if guid in self.__schedule.keys():
            del self.__schedule[guid]
        logging.debug('Unregistering task %s' % guid)

    def run(self):
        while not self.__stopEvent.wait(5):
            try:
                self.reg_all_tasks()
                tick = datetime.now()
                for guid, task_item in self.__schedule.items():
                    if task_item.can_start():
                        logging.debug('Start execution of task %s' % guid)
                        task_item.update_schedule()
                        task = self.__taskManager.get_task(guid)
                        self.__executor.run_sync(task)
                        # task.start()
            except SystemExit:
                self.__stopEvent.set()
            except Exception:
                logging.critical(traceback.format_exc())