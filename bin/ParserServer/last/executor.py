# -*- coding: utf-8 -*-
__author__ = 'sony'

from Queue import Queue
import logging

from interfaces import BaseModule


class TaskExecutor(BaseModule):
    def __init__(self):
        self.__pool = {}  # thread pool ctor
        self.__resultQueue = Queue()

    def run_sync(self, task, single_thread=False):
        logging.debug('Execute task %s' % task.get_param('guid'))
        if single_thread:
            pass
        else:
            task.start()

    def run_async(self, single_thread=False):
        pass