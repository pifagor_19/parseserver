__author__ = 'sony'

import logging
from time import time


def profiler(func):
    def inner(*args, **kwargs):
        _start = time()
        res = func(*args, **kwargs)
        _diff = time() - _start
        logging.info('Function %s executed in %s secs' % (func.__name__, _diff))
        return res
    return inner