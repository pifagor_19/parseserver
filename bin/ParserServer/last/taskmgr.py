__author__ = 'sony'

import uuid
import logging
import inspect
import sys

from container import DataContainer
from interfaces import BaseModule
from task import *

REGISTERED_CLASSES = {}


def get_class(class_name):
    return REGISTERED_CLASSES[class_name] if class_name in REGISTERED_CLASSES.keys() else None


def import_modules():
    module_name = 'task'
    class_check = lambda member: inspect.isclass(member) and member.__module__ == module_name
    for name, obj in inspect.getmembers(sys.modules[module_name], class_check):
        REGISTERED_CLASSES[name] = obj

import_modules()


class TaskManager(BaseModule):
    required_fields = []
    additional_fields = []

    def __init__(self):
        BaseModule.__init__(self)
        self.__tasks = DataContainer()

    def create_task(self, params=dict()):
        logging.debug("Create new task '%s'" % params['type'])
        task_class = get_class(params['type'])
        assert task_class, 'Unable to register task: unknown task type.'
        if not params:
            return None
        guid = uuid.uuid4()
        task = task_class(guid)
        task.set_param('guid', guid)
        for key, val in params.items():
            task.set_param(key, val)
        self.__tasks.set(guid, task)
        return guid

    def create_chain_task(self, guids=list()):
        logging.debug('Create chain task')
        guid = uuid.uuid4()
        task = TaskChain(guid=guid)
        for subtask_guid in guids:
            subtask = self.get_task(subtask_guid)
            task.add_subtask(subtask_guid, subtask)
        self.__tasks.set(guid, task)

    def remove_task(self, guid):
        logging.debug('Removing task %s' % guid)
        self.__tasks.remove(guid)

    def get_task(self, guid):
        return self.__tasks.get(guid)

    def tasks(self):
        return self.__tasks