# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import json
import traceback

from container import DataContainer
import logging


class ConfigManager(object):
    def __init__(self, conf_dir):
        self.__confLocation = conf_dir
        self.__moduleConfigs = DataContainer()

    def load(self):
        try:
            with open(self.__confLocation, 'r') as config_file:
                config_settings = json.load(config_file)
                for module, config in config_settings.items():
                    modconf = DataContainer()
                    modconf.set('name', module)
                    for option, value in config.items():
                        modconf.set(option, value)
                    self.__moduleConfigs.set(module, modconf)
        except:
            logging.critical("Unable to load configuration files.\n"
                             "Details: %s" % traceback.format_exc())

    def get_config(self, module_name):
        return self.__moduleConfigs.get(module_name)

    def set(self):
        pass