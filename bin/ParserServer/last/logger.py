__author__ = 'sony'

import logging
import sys
from logging import handlers

from logutils.colorize import ColorizingStreamHandler
from settings import LOG_FILE_SIZE, LOG_FILE_NAME, BACKUP_FILE_COUNT, USE_COLORIZED_LOGGER


class RainbowLoggingHandler(ColorizingStreamHandler):
    level_map = {
        logging.DEBUG: ('black', 'cyan', True),
        logging.INFO: ('black', 'green', True),
        logging.WARNING: ('black', 'yellow', True),
        logging.ERROR: ('white', 'red', False),
        logging.CRITICAL: ('red', 'white', True),
    }
    date_format = "%H:%m:%S"
    who_padding = 22
    show_name = True

    def get_color(self, fg=None, bg=None, bold=False):
        params = []
        if bg in self.color_map:
            params.append(str(self.color_map[bg] + 40))
        if fg in self.color_map:
            params.append(str(self.color_map[fg] + 30))
        if bold:
            params.append('1')
        color_code = ''.join((self.csi, ';'.join(params), 'm'))
        return color_code

    def colorize(self, record):
        if record.levelno in self.level_map:
            fg, bg, bold = self.level_map[record.levelno]
        else:
            bg = None
            fg = "white"
            bold = False
        template = [
            self.get_color("black", None, True),
            "%(asctime)s ",
            self.reset,
            self.get_color("white", None, True) if self.show_name else "",
            "[%(levelname)s] " if self.show_name else "",
            "%(padded_who)s",
            self.reset,
            " ",
            self.get_color(bg, fg, bold),
            "%(message)s",
            self.reset,
        ]
        format = "".join(template)
        who = ["(",
               str(getattr(record, "lineno", 0)),
               ") ",
               self.get_color("yellow", None, True),
               getattr(record, "module", ""),
               ":",
               self.get_color("green"),
               getattr(record, "funcName", ""),
               self.get_color("black", None, True),
               self.get_color("cyan")
        ]
        who = "".join(who)
        unformatted_who = getattr(record, "module", "") + getattr(record, "funcName", "") + \
                          ":" + str(getattr(record, "lineno", 0))
        if len(unformatted_who) < self.who_padding:
            spaces = " " * (self.who_padding - len(unformatted_who))
        else:
            spaces = ""
        record.padded_who = who + spaces
        formatter = logging.Formatter(format, self.date_format)
        self.colorize_traceback(formatter, record)
        output = formatter.format(record)
        record.ext_text = None
        return output

    def colorize_traceback(self, formatter, record):
        if record.exc_info:
            record.exc_text = "".join([
                self.get_color("red"),
                formatter.formatException(record.exc_info),
                self.reset,
            ])

    def format(self, record):
        if self.is_tty:
            message = self.colorize(record)
        else:
            message = logging.StreamHandler.format(self, record)
        return message

try:
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    log_format = '%(asctime)s [%(levelname)s] (%(lineno)d) %(module)s:%(funcName)s - "%(message)s"'
    date_format = "%Y-%m-%d %H:%m:%S"
    formatter = logging.Formatter(log_format, date_format)

    if USE_COLORIZED_LOGGER:
        color_logger = RainbowLoggingHandler(sys.stdout)
        root.addHandler(color_logger)
    else:
        simple_logger = logging.StreamHandler()
        simple_logger.setFormatter(formatter)
        root.addHandler(simple_logger)

    rotating_handler = handlers.RotatingFileHandler(LOG_FILE_NAME, maxBytes=LOG_FILE_SIZE, backupCount=BACKUP_FILE_COUNT)
    rotating_handler.setFormatter(formatter)
    root.addHandler(rotating_handler)
except:
    logging.error("Unable to register console handler - perhaps, server running in service mode?")