# -*- coding: utf-8 -*-
__author__ = 'adm1n'


##########################################
# Config file location                   #
##########################################
# path to config
CONFIG_LOCATION = "./configs/config.cfg"


##########################################
# Modem providers section                #
##########################################
# Time interval between locking modem requests
LOCK_DELTA_TIME = 20
# Modem provider type
MODEM_PROVIDER = "wvdial"
# Modem provider specific settings
RECONNECT_SYS = {
    "ppp": [
        dict(
            command='poff 3g',
            timeout=10
        ),
        dict(
            command='pon 3g',
            timeout=10
        )
    ],
    "wvdial": [
        dict(
            command='pkill -1 ppp',
            timeout=10
        )
    ]
}


##########################################
# Logger settings                        #
##########################################
# Log file name
LOG_FILE_NAME = './logs/parser.log'
# Log file size
LOG_FILE_SIZE = 1024 * 1024 * 10
# Number of backup files
BACKUP_FILE_COUNT = 10
# Using colorized logger instead of usual
USE_COLORIZED_LOGGER = True


##########################################
# Web crawler settings                   #
##########################################
# Headers template with required template
PARSER_HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;"
              "q=0.9,image/webp,*/*;"
              "q=0.8",
    "User-Agent": ""
}
# Max page count in web parser loop
MAX_PAGE_COUNT = 5
# From what page shall we start search
START_PAGE = 0


##########################################
# Thread pool settings                   #
##########################################
# Amount of parser threads
PARSE_THREADS_COUNT = 15
# Amount of searcher threads
SEARCH_THREADS_COUNT = 5


##########################################
# Scheduler settings                     #
##########################################
# Time intervals for different tasks (TEMP!!!!)
SCHEDULER_INTERVALS = {
    "update1": 30,
    "send1": 10,
    "update2": 45,
    "send2": 10
}