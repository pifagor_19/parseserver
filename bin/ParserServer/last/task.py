from re import template

__author__ = 'sony'

from collections import OrderedDict
import logging

from webutils import PageOpener, WebPage
from container import DataContainer
from lambdas import *
import settings


class BaseTask(object):
    def __init__(self, guid):
        self._guid = guid
        self._params = DataContainer()

    def set_param(self, param, value):
        self._params.set(param, value)

    def get_param(self, param):
        return self._params.get(param)

    def start(self):
        logging.debug('Start base task')

    def callback(self):
        logging.debug('Call base callback')

    def save(self):
        logging.debug('Save base task')

    def load(self):
        logging.debug('Load base task')


class ShutdownTask(BaseTask):
    def __init__(self, guid):
        BaseTask.__init__(self, guid)

    def start(self):
        logging.debug('Shutdown service')
        raise SystemExit


class TaskChain(BaseTask):
    def __init__(self, guid):
        self.__subtasks = OrderedDict()
        BaseTask.__init__(self, guid=guid)
        self._params.set('runnable', True)

    def add_subtask(self, guid, task):
        assert guid not in self.__subtasks.keys(), 'Subtask already exists'
        assert task, 'Non-task object applied'
        self.__subtasks[guid] = task

    def start(self):
        logging.debug('Start tasks chain')
        for guid, task in self.__subtasks.items():
            task.start()


class AvitoParser(BaseTask):
    def __init__(self, guid):
        BaseTask.__init__(self, guid)
        self.__opener = PageOpener()

    def start(self):
        logging.debug('===<<< Starting avito task >>>===')
        params = self._params.get('params')

        def construct_url():
            url_params = params['url']
            url_template = url_params['template_url']
            placeholders = get_placeholders(url_template)
            url_collect = agg_dict(
                placeholders,
                [url_params[i] for i in placeholders]
            )
            url = url_template.format(**url_collect)
            return url

        template_url = construct_url()
        for page_no in xrange(settings.START_PAGE, settings.MAX_PAGE_COUNT):
            url_to_visit = template_url % page_no
            web_page = self.__opener.open(url_to_visit=url_to_visit)
            logging.debug('Opened search page %s' % url_to_visit)

        logging.debug('===<<< Stop avito task >>>===')