# -*- coding: utf-8 -*-
__author__ = 'sony'

import re

module_name = lambda f: f.__class__.__name__

agg_dict = lambda l1, l2: dict(zip(l1, l2))

get_placeholders = lambda x: re.findall(r'{(\w+)}', x)