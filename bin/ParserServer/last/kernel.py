# -*- coding: utf-8 -*-

__author__ = 'sony'

import logging
from threading import Event

from container import DataContainer
from configmgr import ConfigManager
from taskmgr import TaskManager
from task import *
from scheduler import Scheduler
import settings
from lambdas import *


class Kernel(object):
    def __init__(self):
        self.__config = ConfigManager(settings.CONFIG_LOCATION)
        self.__modules = DataContainer()
        self.__stopEvent = Event()

    def load(self):
        logging.info('Loading modules')
        self._load_config()
        self._create_modules()
        self._set_module_params()

    def _load_config(self):
        logging.info('Loading config')
        self.__config.load()

    def _create_modules(self):
        insert_task = lambda f: self.__modules.set(module_name(f), f)

        # TaskManager module
        task_manager = TaskManager()
        insert_task(task_manager)
        logging.debug('%s created' % module_name(task_manager))

        # Scheduler module
        scheduler = Scheduler(task_manager, self.__stopEvent)
        scheduler.start()
        insert_task(scheduler)
        logging.debug('%s created' % module_name(scheduler))

        # WebParser module

    def _set_module_params(self):
        logging.debug('Set module params')
        for _, module in self.__modules.container.items():
            module_settings = self.__config.get_config(module_name(module))
            module.set_settings(module_settings)

    def get_module(self, _module_name):
        return self.__modules.get(_module_name)

    def test(self):
        task_mgr = self.get_module('TaskManager')
        if task_mgr:
            task_params = dict(
                name='Sample task',
                type='AvitoParser',
                # runnable=False,
                runnable=True,
                schedule=dict(
                    start='17.06',
                    stop='23.59',
                    interval='0.0.30',
                    priority='normal',
                    days=range(0, 7)
                ),
                exec_type='sync',
                params=dict(
                    start_page=0,
                    stop_page=5,
                    url=dict(
                        base_url='https://m.avito.ru',
                        location='moskva',
                        category='kvartiry',
                        action='sdam',
                        filter='user=1&page=%s',
                        template_url='{base_url}/{location}/{category}/{action}?{filter}'
                    ),
                    used_module='avito'
                )
            )
            task_params2 = dict(
                name='Shutdown Task',
                type='ShutdownTask',
                # runnable=False,
                runnable=True,
                schedule=dict(
                    start='17.10',
                    stop='23.59',
                    interval='0.0.30',
                    priority='normal',
                    days=range(0, 8)
                ),
                exec_type='sync',
                params=dict()
            )
            g1 = task_mgr.create_task(params=task_params)
            # g2 = task_mgr.create_task(params=task_params2)
            # task_mgr.create_chain_task(guids=[g1,g2])