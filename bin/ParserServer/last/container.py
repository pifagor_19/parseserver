# -*- coding: utf-8 -*-
__author__ = 'sony'


class DataContainer(object):
    def __init__(self):
        self.__fields = list()
        self.__data = dict()

    def set(self, field, value):
        if not field: return
        self.__data[field] = value

    def get(self, param, def_val=None):
        try:
            if param in self.__data:
                return self.__data[param]
        except:
            return def_val

    def remove(self, param):
        if param in self.__data.keys():
            del self.__data[param]

    @property
    def fields(self):
        return self.__data.keys()

    @property
    def container(self):
        return self.__data

    def __str__(self):
        return str(self.items)