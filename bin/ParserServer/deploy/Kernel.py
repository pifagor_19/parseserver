# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import ConfigManager, WebCrawler, DBManager, settings, sysutils, thread
from filter import FilterData
from notifier import ReportGenerator, EmailSender
import os, time, threading
from datetime import datetime, timedelta
from WebCrawler import DataContainer

#############################################
# Kernel definition section                 #
#############################################

class UserSettings(object):
    def __init__(self):
        pass

    def creds(self):
        pass

class Scheduler(threading.Thread):
    def __init__(self, event, kernel):
        threading.Thread.__init__(self)
        self.__stopEvent = event
        self.__kernel = kernel
        today = datetime.now()
        self.__schedList = {
            "update1": today+ timedelta(minutes=0),
            "send1": today + timedelta(minutes=1),
            # "update2": today + timedelta(minutes=2),
            "send2": today + timedelta(minutes=2)
        }

    def run(self):
        while not self.__stopEvent.wait(5):
            today = datetime.now()
            events = self.__kernel._events()
            for action, stamp in self.__schedList.items():
                if today > stamp:
                    print("Action: %s. Datetime: %s.Now is: %s" % (action, stamp, today))
                    print(self.__schedList)
                    self.__schedList[action] += timedelta(minutes=settings.SCHEDULER_INTERVALS[action])
                    thread.start_new_thread(events[action], ())
                    #events[action]()

class Kernel(object):
    def __init__(self):
        # self.printSplash()
        # sysutils.powerOnIface()
        self.__config = ConfigManager.ConfigManager()
        self.__webcrawler = WebCrawler.WebCrawler("lyubertsy")
        self.__webcrawler.setConfig(self.__config.getConfig(self.__webcrawler.moduleName()))
        self.__webcrawler2 = WebCrawler.WebCrawler("moskva")
        self.__webcrawler2.setConfig(self.__config.getConfig(self.__webcrawler2.moduleName()))
        self.__dbmanager = DBManager.DBManager(driver = "PG")
        self.__dbmanager.setCreds("host=127.0.0.1 dbname=avitoparser user=postgres password=acuario")
        self.__fields = ["guid", "datestamp", "url", "category", "town", "price", "contact", "phone", "address", "title", "description"]
        self.__stopSchedulerEvent = threading.Event()
        self.__scheduler = Scheduler(self.__stopSchedulerEvent, self)
        # ad = self.getExistsData("adverts_lubertsy")
        # self.__alreadySend = list(ad.keys())
        # ad = self.getExistsData("adverts_moscow")
        # self.__alreadySend2 = list(ad.keys())
        self.__alreadySend = list()
        self.__alreadySend2 = list()

    def _events(self):
        return {"update1": self.saveData, "send1": self.sendReport,
                "update2": self.saveData2, "send2": self.sendReport2}

    def run(self):
        self.__scheduler.start()
        # self.saveData()
        # self.sendReport()

    def saveData2(self):
        table_name = "adverts_moscow"
        exist_data = self.getExistsData(table_name)
        exist_urls = [advert.getParam("url") for advert in exist_data.values()]
        #print(exist_urls)
        self.__webcrawler2.setExistUrls(exist_urls)
        urls = self.__webcrawler2._collect(settings.MAX_PAGE_COUNT)
        advert = self.__webcrawler2._parse(urls)
        for _, ad in advert.items:
            fields = ad.getFields()
            values = [ad.getParam(field) for field in fields]
            try:
                sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                    table = table_name,
                    keys = ",".join(fields),
                    values = ", ".join(["%s" for i in fields])
                )
                self.__dbmanager.fetch(sql_string, values, returnData=False)
            except:
                print("Unable to add this ad - already exists")
        print("Data import to DB completed")

    def saveData(self):
        table_name = "adverts_lubertsy"
        print(" ___________ Get exist data")
        exist_data = self.getExistsData(table_name)
        exist_urls = [advert.getParam("url") for advert in exist_data.values()]
        print(exist_urls)
        self.__webcrawler.setExistUrls(exist_urls)
        print(" ___________ Parse search")
        urls = self.__webcrawler._collect(settings.MAX_PAGE_COUNT)
        print(" ___________ Parse page")
        advert = self.__webcrawler._parse(urls)
        print(" ___________ Database issues")
        success = 0
        failure = 0
        for _, ad in advert.items:
            fields = ad.getFields()
            values = [ad.getParam(field) for field in fields]
            try:
                sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                    table = table_name,
                    keys = ",".join(fields),
                    values = ", ".join(["%s" for i in fields])
                )
                self.__dbmanager.fetch(sql_string, values, returnData=False)
                success += 1
            except:
                print("Unable to add this ad - already exists")
                failure += 1
        print("Data import to DB completed. Inserted items: %d. Failed items: %d" % (success, failure))

    def getExistsData(self, table_name):
        sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) BETWEEN '%s' AND '%s' ORDER BY guid;" \
                             % (", ".join(self.__fields), table_name, (datetime.now()- timedelta(days=1)).strftime("%Y-%m-%d"), datetime.now().strftime("%Y-%m-%d"))
        #sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) = '%s' ORDER BY guid;" \
        #                     % (", ".join(self.__fields), table_name, datetime.now().strftime("%Y-%m-%d"))
        print(sql_string)
        data = self.__dbmanager.fetch(sql_string)
        advert_contaner = dict()
        for row in data:
            advert = DataContainer()
            param_ctr = 0
            for item in row:
                field = self.__fields[param_ctr]
                value = datetime.strptime(str(item), "%Y-%m-%d %H:%M:%S") if (field == "datestamp") else item
                advert.setParam(field, value)
                param_ctr += 1
            advert_contaner[advert.getParam("guid")] = advert
        return advert_contaner

    def sendReport(self):
        table_name = "adverts_lubertsy"
        advert_contaner = self.getExistsData(table_name)
        #print(advert_contaner)
        only_new = dict()
        for k,v in advert_contaner.items():
            if k not in self.__alreadySend \
                    and v.getParam("datestamp").strftime("%Y-%m-%d") \
                            == datetime.now().strftime("%Y-%m-%d"):
                only_new[k] = v
        filter = FilterData(advert_contaner)
        filter_seq = ['(%s.+?)' % i for i in filter.variation([u"фото", u"номер", "комиссия"])]
        filter_seq.append(u'/телеф\W')
        filtered_ads = filter.select("lambda f: len(re.findall('^8999', f.getParam('phone'))) > 0 "
                              "or len(re.findall('%s', f.getParam('description'))) > 0 "
                              "or len(f.getParam('description')) < 35" % "|".join(filter_seq))
        #print(only_new)
        if len(only_new):
            excelExporter = ReportGenerator(only_new, "Lybertsy")
            excelExporter.create(filtered_ads)
            mailer = EmailSender()
            file_name = excelExporter.getReportName()
            mailer.sendEmail("infoservicebot@gmail.com",
                      ["julievstout@gmail.com", "parser_lub@ask-broker.ru", "topifagorsend@gmail.com"],
                      #["savinkovauv@gmail.com", "julievstout@gmail.com"],
                      #["topifagorsend@gmail.com"],
                      "infoservicebot@gmail.com",
                      "b0tIuAlDm",
                      [file_name])
        for k,v in advert_contaner.items():
            if k not in self.__alreadySend:
                self.__alreadySend.append(k)

    def sendReport2(self):
        table_name = "adverts_moscow"
        advert_contaner = self.getExistsData(table_name)
        #print(advert_contaner)
        only_new = dict()
        for k,v in advert_contaner.items():
            if k not in self.__alreadySend2 \
                    and v.getParam("datestamp").strftime("%Y-%m-%d") \
                            == datetime.now().strftime("%Y-%m-%d"):
                only_new[k] = v
        filter = FilterData(advert_contaner)
        filter_seq = ['(%s.+?)' % i for i in filter.variation([u"фото", u"номер", u"аренда", u"Агент", u"сайт", u"агент"])]
        filter_seq.append(u'(/телеф\\W)')
        # filtered_ads = filter.select("lambda f: len(re.findall('^\\d999', f.getParam('phone'))) > 0 "
        #                       "or len(re.findall('%s', f.getParam('description'))) > 0 "
        #                       "or len(f.getParam('description')) < 35"
        #                       "or len(re.findall('\\d+', f.getParam('contact'))) > 0" % "|".join(filter_seq))
        filtered_ads = filter.select("lambda f: len(re.findall('^\\d999', f.getParam('phone'))) > 0 or "
                                     "len(re.findall('%s', f.getParam('description'))) > 0 "
                              #"or len(f.getParam('description')) < 35"
                              #"or len(re.findall((\\d+\\W), f.getParam('address').lower())) > 0"
                                     % "|".join(filter_seq))
        #print(only_new)
        if len(only_new):
            excelExporter = ReportGenerator(only_new, "Moscow")
            excelExporter.create(filtered_ads)
            mailer = EmailSender()
            file_name = excelExporter.getReportName()
            mailer.sendEmail("infoservicebot@gmail.com",
                      ["julievstout@gmail.com", "parser_msk@ask-broker.ru", "topifagorsend@gmail.com"],
                      #["savinkovauv@gmail.com", "julievstout@gmail.com"],
                      #["topifagorsend@gmail.com"],
                      "infoservicebot@gmail.com",
                      "b0tIuAlDm",
                      [file_name])
        for k,v in advert_contaner.items():
            if k not in self.__alreadySend2:
                self.__alreadySend2.append(k)

    def printSplash(self):
        os.system("clear")
        top = "".rjust(10, "\n")
        delimiter = "".rjust(50, " ")
        logo =  top + \
                delimiter + "#############################################\n" + \
                delimiter + "#   Avito parser service ver 0.2.0          #\n" + \
                delimiter + "#___________________________________________#\n" + \
                delimiter + "#          Author: Alex Pronin              #\n" + \
                delimiter + "#      mailto:topifagorsend@gmail.com       #\n" + \
                delimiter + "#___________________________________________#\n" + \
                delimiter + "#       All rights are reserved.(С)         #\n" + \
                delimiter + "#############################################\n"
        print(logo)
        time.sleep(3)
        os.system("clear")
