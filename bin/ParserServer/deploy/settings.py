# -*- coding: utf-8 -*-
__author__ = 'adm1n'

RECONNECT_SYS = {
    "disable": {
        "command": "poff 3g",
        "timeout": 10
    },
    "enable": {
        "command": "pon 3g",
        "timeout": 10
    }
}

CONFIG_LOCATION = "config.cfg"

MAX_PAGE_COUNT = 1#21
START_PAGE = 0

#SCHEDULER_INTERVALS = {"update": 1, "send": 3}
SCHEDULER_INTERVALS = {"update1": 2, "send1": 10, "update2": 1, "send2": 10}
