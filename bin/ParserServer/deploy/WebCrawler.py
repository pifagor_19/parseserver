# -*- coding: utf-8 -*-
__author__ = 'adm1n'

#############################################
# Import section                            #
#############################################

# system packages
import urllib2, traceback, json, os, time, re
from bs4 import BeautifulSoup
from datetime import datetime, timedelta

# custom packages
import sysutils, settings

#############################################
# Advert container section                  #
#############################################

class DataContainer(object):
    def __init__(self):
        self.__fields = list()
        self.__data = dict()

    def setParam(self, field, value):
        if not field: return
        self.__data[field] = value

    def getParam(self, param):
        if param in self.__data:
            return self.__data[param]

    def getFields(self):
        return self.__data.keys()

    def asText(self):
        return json.dumps(self.__data, ensure_ascii = False)

#############################################
# URL custom builder section                #
#############################################

class UrlBuilder(object):
    def __init__(self, town):
        self.__baseurl = "https://m.avito.ru"
        self.__location = town#"lyubertsy"
        self.__category = "kvartiry"
        self.__adtype = "sdam"
        self.__sortfilter = "user=1&page=%d"
        self.__url = self.__baseurl + "/" + \
                     self.__location + "/" + \
                     self.__category + "/" + \
                     self.__adtype + "?" + \
                     self.__sortfilter

    def build(self, loc, cat, adtype, filter):
        pass

    def getUrl(self):
        return self.__url

    def getBaseUrl(self):
        return self.__baseurl



#############################################
# Web crawler section                       #
#############################################

class WebCrawler(object):
    def __init__(self, town):
        self.__headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
        }
        self.__builder = UrlBuilder(town)
        self.__visitedUrls = list()
        self.__maxRetries = 5

    def moduleName(self):
        return "WebCrawler"

    def setConfig(self, config):
        self.__config = config

    def setExistUrls(self, url):
        self.__visitedUrls = url

    def get_url(self, url):
        req = urllib2.Request(self.__builder.getBaseUrl() + url.get("href"), None, self.__headers)
        response = urllib2.urlopen(req, timeout=20)
        return self._getElement(response.read(), all = False, tags = ['a', 'person-action button button-green action-show-number action-link link '])

    def get_date(self, datestamp):
        datestamp = datestamp.text
        today = datetime.now()
        rep_d = {
            "сегодня": today.strftime("%d %b"),
            "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
            "авг.": "Aug",
            "сен.": "Sep",
            "окт.": "Oct",
            "Размещено ": "",
            "в ": ""
        }
        result = datestamp.encode("utf-8")
        for k,v in rep_d.items():
            result = result.replace(k, v)
        d = today.strftime("%Y") + ' ' + result
        return datetime.strptime(d, "%Y %d %b %H:%M")

    def _collect(self, page_limit = None):
        url_list = list()
        url_template = self.__builder.getUrl()
        visited_page = settings.START_PAGE

        def process_page(url_to_visit):
            try:
                print("Visiting page %s" % url_to_visit)
                request = urllib2.Request(url_to_visit, None, self.__headers)
                t = time.time()
                responce =urllib2.urlopen(request, timeout=20)
                print("Estimated time for parse search page is: %d" % (time.time() - t))
                self._modHeader(responce, url_to_visit)
                for item in self._getElement(responce.read(), all = True, tags = ["a"]):
                    url = self.__builder.getBaseUrl() + item.get("href")
                    if url not in self.__visitedUrls and url \
                            not in url_list and len(url.split("._")) == 2:
                        url_list.append(url)
            except:
                print("IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
                sysutils.sysReconnect()
                process_page(url_to_visit)

        try:
            while True:
                if page_limit and visited_page == page_limit: raise Exception
                url = url_template % visited_page
                process_page(url)
                visited_page += 1
        except: pass
        return url_list

    def _parse(self, url_list):
        data = dict()

        def process_page(url_to_visit):
            try:
                print("Crawling page %s" % url_to_visit)
                self._modHeader(None, url_to_visit)
                request = urllib2.Request(url_to_visit, None, self.__headers)
                t = time.time()
                responce = urllib2.urlopen(request, timeout=20)
                print("Estimated time for parse web page is: %d" % (time.time() - t))
                self._modHeader(responce, url_to_visit)
                if self.__config:
                    page_content = responce.read()
                    advert = DataContainer()
                    for rname, rule in self.__config["content"].items:
                        tags = self._getElement(page_content, all = False, tags = rule["tags"])
                        result = eval(rule["rule"])(tags)
                        advert.setParam(rname, result)
                    advert.setParam("url", url_to_visit)
                    t1 = time.time()
                    phone = self.get_url(self._getElement(page_content, all = False, tags = ["a", "person-action button button-blue action-show-number action-link link "]))
                    print("Estimated time for parse phone page is: %d" % (time.time() - t1))
                    advert.setParam("phone", phone.get('href').replace('tel:', '').replace('-','').replace(' ', ''))
                    datestamp = self.get_date(self._getElement(page_content, all=False, tags=["div", "item-add-date"]))
                    advert.setParam("datestamp", datestamp)
                    data[advert.getParam("guid")] = advert
                else:
                    print("Empty config. Do nothing.")
                print("Estimated time for overall parse web page is: %d" % (time.time() - t))
            except urllib2.HTTPError, e:
                print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
                sysutils.sysReconnect()
                process_page(url_to_visit)
            except:
                print(traceback.format_exc())
                time.sleep(5)
                process_page(url_to_visit)

        for urlpage in url_list:
            process_page(urlpage)

        return data

    def _modHeader(self, responce, urllink):
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        self.__headers["Cookie"] = cookie

    def _getElement(self, page, all = False, tags = []):
        if not page or not tags: return
        parser = BeautifulSoup(page)
        res = None
        if all: res = parser.find_all(*tags)
        else: res = parser.find(*tags)
        return res