# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import os, settings, time

def sysReconnect():
    _executeCommand(**settings.RECONNECT_SYS["disable"])
    _executeCommand(**settings.RECONNECT_SYS["enable"])

def powerOnIface():
    print("Power on network interface ...")
    _executeCommand(**settings.RECONNECT_SYS["enable"])
    print("[OK] Network interface is switched on.")

def _executeCommand(**commands):
    os.system(commands["command"])
    time.sleep(commands["timeout"])
