# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import itertools, re

#############################################
# Filer section                             #
#############################################

class FilterData(object):
    def __init__(self, data):
        self.__data = data
        self.__rep_dict = {
            u"а" : "a",
            u"е" : "e",
            u"о" : "o",
            u"у" : "y",
            u"р" : "p",
            u"к" : "k",
            u"х" : "x",
            u"с" : "c"
        }

    def select(self, criteria):
        ret_res = {}
        true_case = 0
        false_case = 0
        for guid, advert in self.__data.items:
            res = eval(criteria)(advert)
            ret_res[guid] = res
            if res: true_case += 1
            else: false_case += 1
        print("Fake adverts: %d, Real adverts: %d, Total count: %d" % (true_case, false_case, len(self.__data)))
        return ret_res

    def gen_query(self, input_dict):
        output = list()
        for word, r_dict in input_dict.items:
            for item in itertools.product([0,1], repeat = len(r_dict)):
                m_word = list(word)
                ctr = 0
                for i in item:
                    if i: m_word[r_dict.keys()[ctr]] = self.__rep_dict[m_word[r_dict.keys()[ctr]]]
                    ctr += 1
                output.append("".join(m_word))
        return output

    def variation(self, input):
        words_rep = dict()
        for item in input:
            pos = 0
            r_dict = dict()
            for letter in item:
                if letter in self.__rep_dict.keys():
                    r_dict[pos] = letter
                pos += 1
            if r_dict:
                words_rep[item] = r_dict
        return self.gen_query(words_rep)
