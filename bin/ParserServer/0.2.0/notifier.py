# -*- coding: utf-8 -*-

__author__ = 'adm1n'

import xlsxwriter, smtplib, os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import formatdate
from email import Encoders
from datetime import datetime
import time

#############################################
# Report generator section                  #
#############################################

class ReportGenerator(object):
    def __init__(self, data, town):
        self.__data = data
        self.__fileName = town + "_" + datetime.now().strftime("%Y_%m_%d_%H_%M") + ".xlsx"
        self.__workbook = xlsxwriter.Workbook(self.__fileName)
        if os.path.isfile(self.__fileName):
            os.remove(self.__fileName)
        self.__formats = {
            "bold": self.__workbook.add_format({'bold': 2}),
            "valid": self.__workbook.add_format({'bg_color': '#90EE90'}),
            "invalid": self.__workbook.add_format({'bg_color': '#E66761'})
        }
        self.__worksheet = self.__workbook.add_worksheet()
        self.__fields = ["guid", "datestamp", "url", "category", "town", "price", "contact", "phone", "address", "title", "description"]

    def create(self, filtered_ads = {}):
        column_count = 0
        for item in self.__fields:
            self.__worksheet.write(0, column_count, item, self.__formats["bold"])
            self.__worksheet.set_column(0, column_count, 15)
            column_count += 1
        row_count = 1
        for advert in self.__data.values():
            column_count = 0
            guid = advert.getParam("guid")
            format_guid = None
            if filtered_ads and filtered_ads[guid]:
                format_guid = self.__formats["invalid"]
            else:
                format_guid = self.__formats["valid"]
            for item in self.__fields:
                if item == "url":
                    self.__worksheet.write_url(row_count, column_count, advert.getParam(item))
                elif item == "datestamp":
                    self.__worksheet.write_string(row_count, column_count, advert.getParam(item).strftime("%H:%M %d.%m.%Y "))
                else:
                    if item != "guid":
                        format_guid = None
                    try:
                        self.__worksheet.write_string(row_count, column_count, advert.getParam(item).decode("utf-8"), format_guid)#.decode("utf-8")
                    except:
                        self.__worksheet.write_string(row_count, column_count, unicode(advert.getParam(item)), format_guid)
                column_count += 1
            row_count += 1
        self.__worksheet.autofilter(0, 0, len(self.__data)-1, len(self.__fields)-1)
        self.__workbook.close()

    def getReportName(self):
        return self.__fileName

#############################################
# Kerne definition section                  #
#############################################

class EmailSender(object):
    def __init__(self):
        self.__template = {
            "body": "    Здравствуйте! Вы получили это письмо, потому что зарегистрированы " +
                "на сервисе оповещения о новых рекламных сообщениях. Не отвечайте на это " +
                "письмо - оно автоматически сгенерировано ботом." + "\n\n" +
                "    Hello! You received this message, because you have subscribed to notification " +
                "on new adverts on site avito.ru. Do not respond back to this message - it is " +
                "automatically generated.".encode("utf-8"),
            "subject": u"Рассылка уведомлений по почте".encode("utf-8")
        }

    def sendEmail(self, fromEmail, toEmail, login, passwd, fileName):
        for emailAddr in toEmail:
            self.send(fromEmail, emailAddr, login, passwd, fileName)

    def send(self, fromEmail, toEmail, login, passwd, fileName):
        msg = MIMEMultipart()
        msg['Subject'] = self.__template["subject"]
        msg['From'] = fromEmail
        msg['To'] = toEmail
        msg['Date'] = formatdate(localtime=True)
        msg.attach( MIMEText(self.__template["body"], _charset='utf-8') )
        try:
            for f in fileName:
                part = MIMEBase('application', "octet-stream")
                part.set_payload( open(f,"rb").read() )
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
                msg.attach(part)

            server = smtplib.SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(login,passwd)
            server.sendmail(fromEmail, toEmail, msg.as_string())
            server.quit()
            print("[OK] Email %s successfully sent" % toEmail)
        except Exception as e:
            print "[ERROR] Something went wrong when sending the email %s" % fromEmail
            print e
            time.sleep(5)
            self.send(fromEmail, toEmail, login, passwd, fileName)