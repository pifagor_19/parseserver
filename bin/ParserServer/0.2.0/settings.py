# -*- coding: utf-8 -*-
__author__ = 'adm1n'

RECONNECT_SYS = {
    "ppp": [
        {
            "command": "poff 3g",
            "timeout": 10
        },
        {
            "command": "pon 3g",
            "timeout": 10
        }
    ],
    "wvdial": [
        {
            "command": "pkill -1 ppp",
            "timeout": 10
        }
    ]
}

CONFIG_LOCATION = "config.cfg"
MODEM_PROVIDER = "wvdial"
MAX_PAGE_COUNT = 5
START_PAGE = 0
PARSE_THREADS_COUNT = 15
SEARCH_THREADS_COUNT = 5
LOCK_DELTA_TIME = 20

#SCHEDULER_INTERVALS = {"update": 1, "send": 3}
SCHEDULER_INTERVALS = {"update1": 30, "send1": 10, "update2": 45, "send2": 10}