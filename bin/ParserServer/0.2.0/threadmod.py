# -*- coding: utf-8 -*-
from _socket import timeout

__author__ = 'admin'

import threading, time
from Queue import Queue, Empty
from threading import Event

class ParserQueue(Queue):
    def _init(self, maxsize):
        Queue._init(self, maxsize)
        self.all_items = list()

    def _put(self, item):
        if item not in self.all_items:
            Queue._put(self, item)
            self.all_items.append(item)#add(item)

    def _get(self):
        Queue._get(self)
        return self.all_items.pop()

class Worker(threading.Thread):
    def __init__(self, tasks, event):
        threading.Thread.__init__(self)
        self.tasks = tasks
        self.event = event
        self.daemon = True
        print("Thread born")
        self.start()

    def __del__(self):
        print("Thread dtor()")

    def resetThread(self):
        print("Resetting thread")
        self.__del__()

    def run(self):
        while True:
            try:
                func, args, kargs = self.tasks.get(timeout=30)
                func(*args, **kargs)
            except Empty:
                print("Empty task list")
                # self.resetThread()
            except Exception, e:
                print e
            finally:
                if not self.isAlive():
                    print("Thread died")
                    self.__del__()
                else:
                    print("Thread alive")
                try:
                    self.tasks.task_done()
                except ValueError: pass
                print("Task done")

class ThreadPool:
    def __init__(self, num_threads):
        self.stopEvent = Event()
        self.tasks = Queue(num_threads)
        # self.tasks = ParserQueue(num_threads)
        self.workers = list()
        for _ in range(num_threads):
            w = Worker(self.tasks, self.stopEvent)
            self.workers.append(w)
        t = threading.Thread(target=self.check_threads)
        t.start()
        # t.join()

    def add_task(self, func, *args, **kargs):
        self.tasks.put((func, args, kargs))

    def wait_completion(self):
        self.tasks.join()

    def check_threads(self):
        while True:
            print("Checking threads...")
            print("Queue size is: %d" % len(self.tasks.queue))
            # print("Queue size is: %d" % len(self.tasks.all_items))
            self.stopEvent.set()
            # for thread in self.workers:
            #     thread.resetThread()
                # if not thread.isAlive():
                #     print("Dead thread found...")
                #     thread.resetThread()

            time.sleep(20)