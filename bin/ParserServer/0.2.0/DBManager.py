# -*- coding: utf-8 -*-
__author__ = 'adm1n'
import psycopg2, traceback
#############################################
# Driver interface section                  #
#############################################

class DriverInterface(object):
    def __init__(self):
        pass

    def connString(self, conn_string):
        pass

    def connect(self):
        pass

    def fetch(self, query, retData = False):
        pass

    def disconnect(self):
        pass

#############################################
# PG driver section                         #
#############################################

class PGDBDriver(DriverInterface):
    def __init__(self):
        super(PGDBDriver, self).__init__()

#############################################
# Driver enum section                       #
#############################################

DB_DRIVERS = {
    "PG": PGDBDriver
}

#############################################
# Database manager section                  #
#############################################

class DBManager(object):
    def __init__(self, driver = "PG"):
        self.__credentials = ""
        self.__connection = None
        if driver in DB_DRIVERS:
            self.__driver = DB_DRIVERS[driver]()
        else:
            raise Exception("Unable to create unknown driver type")
        print("DBManagr ctor()")

    def setCreds(self, params):
        self.__credentials = params

    def connect(self):
        if self.__connection: return
        try:
            if not self.__credentials:
                print("[ERROR] No credentials specified. Exit...")
                return
            self.__connection = psycopg2.connect(self.__credentials)
            self.__connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        except Exception as e:
            print(traceback.format_exc())

    def fetch(self, sqlQuery, inputData = None, returnData = True, fetchOne = False):
        data = None
        self.connect()
        cursor = self.__connection.cursor()
        try:
            cursor.execute(sqlQuery) if not inputData else cursor.execute(sqlQuery, inputData)
        except Exception:
            print("Unable to execute query.")
            raise Exception
        if returnData:
            data = cursor.fetchall() if not fetchOne else cursor.fetchone()
        return data
