from test_scripts.filter import select

__author__ = 'pronin'

#############################################
# Exceptions section                        #
#############################################

class BaseException(object):
    def __init__(self):
        pass

class ConfigException(BaseException):
    def __init__(self):
        pass

class PluginException(BaseException):
    def __init__(self):
        pass

class KernelException(BaseException):
    def __init__(self):
        pass

#############################################
# Interfaces section                        #
#############################################

class BaseInterface(object):
    def __init__(self):
        self.__data = {}

    def set(self, param, value, overwrite = True):
        if overwrite:
            self.__data[param] = value

    def get(self, param):
        return self.__data[param]

class Advert(BaseInterface):
    def __init__(self):
        self.__fields = list()
        super(Advert, self).__init__()

    def get_fields(self):
        return self.__fields

    def set_fields(self, *fields):
        self.__fields = fields

    def set_field(self, field):
        if field not in self.__fields:
            self.__fields.append(field)

class Kernel(object):
    def __init__(self):
        self.__adverts = {}
        self.__plugins = {}

    def load(self):
        pass

class WebCrawler(BaseInterface):
    def __init__(self):
        pass

