# -*- coding: utf-8 -*-
__author__ = 'adm1n'

#############################################
# Import section                            #
#############################################

# system packages
import urllib2, traceback, json, os, time, re
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from threading import Lock, Thread, Event
from threadmod import ThreadPool
import copy, random, cookielib
# custom packages
import sysutils, settings

#############################################
# Advert container section                  #
#############################################

class DataContainer(object):
    def __init__(self):
        self.__fields = list()
        self.__data = dict()

    def setParam(self, field, value):
        if not field: return
        self.__data[field] = value

    def getParam(self, param):
        if param in self.__data:
            return self.__data[param]

    def getFields(self):
        return self.__data.keys()

    def asText(self):
        return json.dumps(self.__data, ensure_ascii = False)

#############################################
# URL custom builder section                #
#############################################

class UrlBuilder(object):
    def __init__(self, town):
        self.__baseurl = "https://m.avito.ru"
        self.__location = town#"lyubertsy"
        self.__category = "kvartiry"
        self.__adtype = "sdam"
        self.__sortfilter = "user=1&page=%d"
        self.__url = self.__baseurl + "/" + \
                     self.__location + "/" + \
                     self.__category + "/" + \
                     self.__adtype + "?" + \
                     self.__sortfilter

    def build(self, loc, cat, adtype, filter):
        pass

    def getUrl(self):
        return self.__url

    def getBaseUrl(self):
        return self.__baseurl



#############################################
# Web crawler section                       #
#############################################

class WebCrawler(object):
    def __init__(self, town):
        self.__headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
        }
        self.__builder = UrlBuilder(town)
        self.__visitedUrls = list()
        self.__maxRetries = 5

    def moduleName(self):
        return "WebCrawler"

    def setConfig(self, config):
        self.__config = config

    def setExistUrls(self, url):
        self.__visitedUrls = url

    def get_url(self, url):
        req = urllib2.Request(self.__builder.getBaseUrl() + url.get("href"), None, self.__headers)
        response = urllib2.urlopen(req)
        return self._getElement(response.read(), all = False, tags = ['a', 'person-action button button-green action-show-number action-link link '])

    def get_date(self, datestamp):
        datestamp = datestamp.text
        today = datetime.now()
        rep_d = {
            "сегодня": today.strftime("%d %b"),
            "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
            "авг.": "Aug",
            "сен.": "Sep",
            "Размещено ": "",
            "в ": ""
        }
        result = datestamp.encode("utf-8")
        for k,v in rep_d.items():
            result = result.replace(k, v)
        d = today.strftime("%Y") + ' ' + result
        return datetime.strptime(d, "%Y %d %b %H:%M")

    def _collect(self, page_limit = None):
        url_list = list()
        url_template = self.__builder.getUrl()
        visited_page = settings.START_PAGE

        def process_page(url_to_visit, attempts):
            try:
                print("Visiting page %s" % url_to_visit)
                request = urllib2.Request(url_to_visit, None, self.__headers)
                t = time.time()
                responce =urllib2.urlopen(request)
                print("Estimated time for parse search page is: %d" % (time.time() - t))
                self._modHeader(responce, url_to_visit)
                for item in self._getElement(responce.read(), all = True, tags = ["a"]):
                    url = self.__builder.getBaseUrl() + item.get("href")
                    if url not in self.__visitedUrls and url \
                            not in url_list and len(url.split("._")) == 2:
                        url_list.append(url)
            except urllib2.HTTPError, e:
                print("IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
                sysutils.sysReconnect()
                process_page(url_to_visit, attempts)
            except:
                if attempts == 5: return
                attempts += 1
                time.sleep(5)
                process_page(url_to_visit, attempts)

        try:
            while True:
                if page_limit and visited_page == page_limit: raise Exception
                url = url_template % visited_page
                process_page(url, 0)
                visited_page += 1
        except: pass
        return url_list

    def _parse(self, url_list, dbmanager, table_name):
        data = dict()

        def process_page(url_to_visit):
            try:
                print("Crawling page %s" % url_to_visit)
                self._modHeader(None, url_to_visit)
                request = urllib2.Request(url_to_visit, None, self.__headers)
                t = time.time()
                responce = urllib2.urlopen(request)
                print("Estimated time for parse web page is: %d" % (time.time() - t))
                self._modHeader(responce, url_to_visit)
                if self.__config:
                    page_content = responce.read()
                    advert = DataContainer()
                    for rname, rule in self.__config["content"].items:
                        tags = self._getElement(page_content, all = False, tags = rule["tags"])
                        result = eval(rule["rule"])(tags)
                        advert.setParam(rname, result)
                    advert.setParam("url", url_to_visit)
                    t1 = time.time()
                    phone = self.get_url(self._getElement(page_content, all = False, tags = ["a", "person-action button button-blue action-show-number action-link link "]))
                    print("Estimated time for parse phone page is: %d" % (time.time() - t1))
                    advert.setParam("phone", phone.get('href').replace('tel:', '').replace('-','').replace(' ', ''))
                    datestamp = self.get_date(self._getElement(page_content, all=False, tags=["div", "item-add-date"]))
                    advert.setParam("datestamp", datestamp)
                    data[advert.getParam("guid")] = advert
                else:
                    print("Empty config. Do nothing.")
                print("Estimated time for overall parse web page is: %d" % (time.time() - t))
                fields = advert.getFields()
                values = [advert.getParam(field) for field in fields]
                try:
                    sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                        table = table_name,
                        keys = ",".join(fields),
                        values = ", ".join(["%s" for i in fields])
                    )
                    dbmanager.fetch(sql_string, values, returnData=False)
                except:
                    print("Unable to add this ad - already exists")
            except urllib2.HTTPError, e:
                print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
                sysutils.sysReconnect()
                process_page(url_to_visit)
            except:
                print(traceback.format_exc())
                time.sleep(5)
                #process_page(url_to_visit)

        for urlpage in url_list:
            process_page(urlpage)

        return data

    def _modHeader(self, responce, urllink):
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        self.__headers["Cookie"] = cookie

    def _getElement(self, page, all = False, tags = []):
        if not page or not tags: return
        parser = BeautifulSoup(page)
        res = None
        if all: res = parser.find_all(*tags)
        else: res = parser.find(*tags)
        return res

PARSER_HEADERS = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            #"Cookie": "",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
        }

class WebCrawler2(object):
    def __init__(self):
        self.__pool = ThreadPool(settings.THREADS_COUNT)
        self.__visitedUrls = list()
        self.__retRes = dict()
        self.__headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            #"Cookie": "",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
        }

    def moduleName(self):
        return "WebCrawler"

    def setConfig(self, config):
        self.__config = config

    def setExistUrls(self, url):
        for item in url:
            if item in self.__visitedUrls:
                self.__visitedUrls.append(url)

    def _events(self):
        return {"update": self.put_task}

    def put_task(self, town):
        retRes = dict()
        page_template = "https://m.avito.ru/" + town + "/kvartiry/sdam?user=1&page=%d"
        for page in xrange(settings.START_PAGE, settings.MAX_PAGE_COUNT):
            self.__pool.add_task(self.parse_search, page_template % page, retRes)
        self.__pool.wait_completion()
        print("Execution completed")
        return retRes

    def get_url(self, url, fromUrl):
        global PARSER_HEADERS
        # time.sleep(random.randint(0,5))
        base_url = "https://m.avito.ru"
        url_to_visit = base_url + url.get("href") + "?async"
        print("Check system: %d" % url_to_visit.find(fromUrl))

        # header = copy.deepcopy(PARSER_HEADERS)
        # header["Referer"] = fromUrl
        self._modHeader(None, fromUrl)
        _result = self._open(url_to_visit)
        print(_result)
        obj = json.loads(_result)
        print(obj["phone"])
        return obj["phone"]

        # try:
        #     # print("-----------------------------------------")
        #     print("Opening page %s" % (url_to_visit))
        #     request = urllib2.Request(url_to_visit, None, header)
        #     t = time.time()
        #     responce = urllib2.urlopen(request)
        #     print("Timeout is: %d" % (time.time() - t))
        #     _result = responce.read()
        #     self._modHeader(responce, url_to_visit)
        #     print(_result)
        #     parser = BeautifulSoup(_result)
        #     div = parser.find('a', 'person-action button button-green action-show-number action-link link ')
        #     ph = div.get('href')
        #     # return json.loads(_result)["phone"]
        #     return div
        # except urllib2.HTTPError, e:
        #     print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
        #     sysutils.sysReconnect()
        #     raise Exception
        # except:
        #     print("Exception on page %s. Details: %s.\nHeaders: %s" % (url_to_visit, traceback.format_exc(), header))
        #     raise Exception

    def _modHeader(self, responce, urllink):
        global PARSER_HEADERS
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        if "Cookie" not in self.__headers.keys() or not self.__headers["Cookie"]:
            self.__headers["Cookie"] = cookie

    def _open(self, url_to_visit):
        global PARSER_HEADERS
        try:
            # print("-----------------------------------------")
            print("Opening page %s" % url_to_visit)
            request = urllib2.Request(url_to_visit, None, self.__headers)
            t = time.time()
            responce = urllib2.urlopen(request, timeout=20)
            print("Timeout is: %d" % (time.time() - t))
            self._modHeader(responce, url_to_visit)
            return responce.read()
        except urllib2.HTTPError, e:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            sysutils.sysReconnect()
            raise Exception

        except:
            # print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            print("Exception on search page %s. Details: %s" % (url_to_visit, traceback.format_exc()))
            # sysutils.sysReconnect()
            raise Exception

    def parse_search(self, search_url, retRes):
        try:
            base_url = "https://m.avito.ru"
            print(search_url)
            _result = self._open(search_url)
            if not _result:
                raise Exception
            parser = BeautifulSoup(_result)
            tags = parser.find_all("article")
            for item in tags:
                url = base_url + item.a.get("href")
                datetamp = item.find("div", "info-date info-text").text.replace("\n", "").replace(",", "")
                if len(url.split("._")) == 2 and url not in self.__visitedUrls:
                    self.__pool.add_task(self.parse_advert, url, retRes)
        except Exception:
            print("Exception on search page %s. Details: %s" % (search_url, traceback.format_exc()))
            time.sleep(2)
            self.__pool.add_task(self.parse_search, search_url, retRes)

    def parse_advert(self, advert_url, retRes):
        print("Parse advert: %s" % advert_url)
        try:
            self._modHeader(None, advert_url)
            _result = self._open(advert_url)
            if not _result:
                raise Exception
            if self.__config:
                advert = DataContainer()
                for rname, rule in self.__config["content"].items:
                    tags = self._getElement(_result, all = False, tags = rule["tags"])
                    result = eval(rule["rule"])(tags)
                    advert.setParam(rname, result)
                advert.setParam("url", advert_url)
                # t1 = time.time()
                phone = self.get_url(self._getElement(_result, all = False, tags = ["a", "person-action button button-blue action-show-number action-link link "]), advert_url)
                # print("Estimated time for parse phone page is: %d" % (time.time() - t1))
                advert.setParam("phone", phone.replace('tel:', '').replace('-','').replace(' ', ''))
                datestamp = self.get_date(self._getElement(_result, all=False, tags=["div", "item-add-date"]))
                advert.setParam("datestamp", datestamp)
                retRes[advert.getParam("guid")] = advert
            else:
                print("Empty config. Do nothing.")

            if advert_url not in self.__visitedUrls:
                print(len(self.__visitedUrls))
                self.__visitedUrls.append(advert_url)
        except Exception:
            print("Exception on advert page %s. Details: %s" % (advert_url, traceback.format_exc()))
            time.sleep(2)
            self.__pool.add_task(self.parse_advert, advert_url, retRes)

    def _getElement(self, page, all = False, tags = []):
        if not page or not tags: return
        parser = BeautifulSoup(page)
        res = parser.find_all(*tags) if all else parser.find(*tags)
        return res

    def get_date(self, datestamp):
        datestamp = datestamp.text
        today = datetime.now()
        rep_d = {
            "сегодня": today.strftime("%d %b"),
            "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
            "авг.": "Aug",
            "сен.": "Sep",
            "Размещено ": "",
            "в ": ""
        }
        result = datestamp.encode("utf-8")
        for k,v in rep_d.items():
            result = result.replace(k, v)
        d = today.strftime("%Y") + ' ' + result
        return datetime.strptime(d, "%Y %d %b %H:%M")