# -*- coding: utf-8 -*-

__author__ = 'adm1n'

import threading, time, json, urllib2, traceback, thread, re
from datetime import datetime, timedelta
from Queue import Queue, Empty
from threading import Lock, Thread, Event
from bs4 import BeautifulSoup
from copy import deepcopy
import settings, sysutils
from DBManager import DBManager
from ConfigManager import ConfigManager
import useragent
# -----------------------------------------------------------

PARSER_HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "User-Agent": ""
}
base_url = "https://m.avito.ru"
RULES_SYSTEM = {}
EXISTING_ADVERTS = dict()
table_name = "adverts_moscow"
# -----------------------------------------------------------

class ParserQueue(Queue):
    def _init(self, maxsize):
        Queue._init(self, maxsize)
        self.all_items = list()

    def _put(self, item):
        if item not in self.all_items:
            Queue._put(self, item)
            self.all_items.append(item)#add(item)

    def _get(self):
        Queue._get(self)
        return self.all_items.pop()

# -----------------------------------------------------------

class DataContainer(object):
    def __init__(self):
        self.__fields = list()
        self.__data = dict()

    def setParam(self, field, value):
        if not field: return
        self.__data[field] = value

    def getParam(self, param):
        if param in self.__data:
            return self.__data[param]

    def getFields(self):
        return self.__data.keys()

    def asText(self):
        return json.dumps(self.__data, ensure_ascii = False)

# -----------------------------------------------------------

class UrlBuilder(object):
    def __init__(self, town):
        self.__baseurl = "https://m.avito.ru"
        self.__location = town#"lyubertsy"
        self.__category = "kvartiry"
        self.__adtype = "sdam"
        self.__sortfilter = "user=1&page=%d"
        self.__url = self.__baseurl + "/" + \
                     self.__location + "/" + \
                     self.__category + "/" + \
                     self.__adtype + "?" + \
                     self.__sortfilter

    def build(self, loc, cat, adtype, filter):
        pass

    def getUrl(self):
        return self.__url

    def getBaseUrl(self):
        return self.__baseurl

# -----------------------------------------------------------

class WebPage(object):
    def __init__(self, content):
        self.__content = content
        self.__parsedPage = BeautifulSoup(self.__content)

    def get(self, tags = list(), all = False):
        result = self.__parsedPage.find(*tags) if not all else self.__parsedPage.find_all(*tags)
        return result

    def setExtaData(self, extraData = dict()):
        self.__metaData = deepcopy(extraData)

    def save(self, path = None):
        pass

# -----------------------------------------------------------

class PageOpener(object):
    def __init__(self):
        global PARSER_HEADERS
        self.__headers = deepcopy(PARSER_HEADERS)
        self.__headers["User-Agent"] = useragent.getRandomUserAgent()

    def _modHeader(self, responce, urllink):
        global PARSER_HEADERS
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        if "Cookie" not in self.__headers.keys() or not self.__headers["Cookie"]:
            self.__headers["Cookie"] = cookie

    def open(self, url_to_visit):
        # try:
        # print("Opening page %s" % url_to_visit)
        request = urllib2.Request(url_to_visit, None, self.__headers)
        t = time.time()
        responce = urllib2.urlopen(request, timeout=20)
        # print("Estimated time for page load is: %d" % (time.time() - t))
        self._modHeader(responce, url_to_visit)
        return responce.read()
        # except urllib2.HTTPError, e:
        #     print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
        #     sysutils.sysReconnect()
        #     raise Exception
        # except:
        #     print("Exception on search page %s. Details: %s" % (url_to_visit, traceback.format_exc()))
        #     # sysutils.sysReconnect()
        #     # raise Exception

# -----------------------------------------------------------

# def ExcHandler(func):
#     def insideFunction(*args, **kwargs):
#         try:
#             result = func(*args, **kwargs)
#             return result
#         except Exception as e:
#             print(traceback.format_exc())
#     return insideFunction
#
# def FatalExcHandler(func):
#     def insideFunction(*args, **kwargs):
#         try:
#             result = func(*args, **kwargs)
#             return result
#         except urllib2.HTTPError, e:
#             print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
#             sysutils.sysReconnect()
#     return insideFunction

# -----------------------------------------------------------

class ItemCollector(DataContainer):
    def __init__(self):
        DataContainer.__init__(self)

def get_date(datestamp):
    datestamp = datestamp.text
    today = datetime.now()
    rep_d = {
        "сегодня": today.strftime("%d %b"),
        "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
        "авг.": "Aug",
        "сен.": "Sep",
        "окт.": "Oct",
        "Размещено ": "",
        "в ": ""
    }
    result = datestamp.encode("utf-8")
    for k,v in rep_d.items():
        result = result.replace(k, v)
    d = today.strftime("%Y") + ' ' + result
    return datetime.strptime(d, "%Y %d %b %H:%M")

class ResultThreadParser(Thread):
    def __init__(self, resultQueue, stopEvt, result):
        global RULES_SYSTEM
        Thread.__init__(self)
        self.__resultQueue = resultQueue
        self.__stopEvent = stopEvt
        self.__opener = PageOpener()
        self.daemon = True
        self.__config = deepcopy(RULES_SYSTEM)
        self.__result = result

    def __del__(self):
        print("Deleting thread")

    def get_url(self, url, fromUrl):
        # time.sleep(random.randint(0,5))
        base_url = "https://m.avito.ru"
        url_to_visit = base_url + url.get("href") + "?async"
        print("Check system: %d" % url_to_visit.find(fromUrl))
        self.__opener._modHeader(None, fromUrl)
        _result = self.__opener.open(url_to_visit)
        print(_result)
        obj = json.loads(_result)
        print(obj["phone"])
        return obj["phone"]

    def task(self):
        try:
            search_page = self.__resultQueue.get()
            print("Opening result page %s" % search_page)
            content = self.__opener.open(search_page)
            # print(content)
            webPage = WebPage(content)
            if self.__config:
                advert = DataContainer()
                for rname, rule in self.__config["content"].items:
                    tags = webPage.get(tags=rule["tags"])
                    result = eval(rule["rule"])(tags)
                    advert.setParam(rname, result)
                advert.setParam("url", search_page)
                # t1 = time.time()
                phone = self.get_url(webPage.get(tags=["a", "person-action button button-blue action-show-number action-link link "]), search_page)
                # print("Estimated time for parse phone page is: %d" % (time.time() - t1))
                advert.setParam("phone", phone.replace('tel:', '').replace('-','').replace(' ', ''))
                datestamp = get_date(webPage.get(tags=["div", "item-add-date"]))
                advert.setParam("datestamp", datestamp)
                self.__result.put(advert)

            else:
                print("Empty config. Do nothing.")
        except Empty: pass
        except urllib2.HTTPError as e:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            self.__resultQueue.put(search_page)
            sysutils.sysReconnect()
        except AttributeError: print("Attribute Error!!!")
        except Exception:
            print(traceback.format_exc())
            self.__resultQueue.put(search_page)

        self.__resultQueue.task_done()

    def run(self):
        while not self.__stopEvent.isSet():
            self.task()

class SearchThreadParser(Thread):
    def __init__(self, searchQueue, resulQueue, stopEvt):
        Thread.__init__(self)
        self.__searchQueue = searchQueue
        self.__resultQueue = resulQueue
        self.__stopEvent = stopEvt
        self.__opener = PageOpener()
        self.daemon = True

    def __del__(self):
        print("Deleting thread")

    def task(self):
        global base_url, EXISTING_ADVERTS
        try:
            search_page = self.__searchQueue.get()
            content = self.__opener.open(search_page)
            webPage = WebPage(content)
            adverts = webPage.get(all=True, tags=["article"])
            print("Adverts amount: %d" % len(adverts))
            for ad in adverts:
                _url = base_url + ad.a.get("href")
                if _url not in EXISTING_ADVERTS:
                    self.__resultQueue.put(_url)
                    EXISTING_ADVERTS.append(_url)

        except Empty: pass
        except urllib2.HTTPError:
            print("[ERROR] IP was banned by site. Reconnecting... \nAdditional info: %s" % traceback.format_exc())
            self.__searchQueue.put(search_page)
            sysutils.sysReconnect()
        except Exception:
            print(traceback.format_exc())
            self.__searchQueue.put(search_page)
        self.__searchQueue.task_done()

    def run(self):
        while not self.__stopEvent.isSet():
            self.task()

# -----------------------------------------------------------

class ThreadManager(object):
    def __init__(self, stopEvent):
        self.__seachQueue = Queue()
        self.__resultQueue = Queue()
        self.__returnQueue = Queue()
        self.__searchPool = list()
        self.__resultPool = list()
        self.__event = stopEvent
        self._manage()

    def _manage(self):
        self._create()
        self._start()

    def _create(self):
        for _ in xrange(settings.SEARCH_THREADS_COUNT):
            _st = SearchThreadParser(self.__seachQueue, self.__resultQueue, self.__event)
            self.__searchPool.append(_st)
        for _ in xrange(settings.PARSE_THREADS_COUNT):
            _rt = ResultThreadParser(self.__resultQueue, self.__event, self.__returnQueue)
            self.__resultPool.append(_rt)

    def _start(self):
        for _st in self.__searchPool:
            _st.start()
        for _rt in self.__resultPool:
            _rt.start()

    def put_task(self, task_item):
        self.__seachQueue.put(task_item)

    def _retrieveResult(self):
        result = list()
        while True:
            try:
                result.append(self.__returnQueue.get_nowait())
            except Empty:
                break

        return result

    def wait_lock(self):
        self.__seachQueue.join()
        self.__resultQueue.join()
        result = self._retrieveResult()
        return result

# -----------------------------------------------------------

class RuleSystem(object):
    def __init__(self):
        self.__rules = dict()

    def load(self, **kwargs):
        for k,v in kwargs.items():
            self.__rules[k] = v

    def get(self):
        pass

# -----------------------------------------------------------

class WebParser(object):
    def __init__(self, dbManager):
        self.__stopEvent = Event()
        self.__threadManager = ThreadManager(self.__stopEvent)
        self.__dbManager = dbManager

    def _events(self):
        return {"update": self.put_task}

    def __del__(self):
        self.__stopEvent.set()

    def load_db(self):
        global EXISTING_ADVERTS, table_name

        sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) BETWEEN '%s' AND '%s' ORDER BY guid;" \
                             % ("guid", table_name, (datetime.now()- timedelta(days=1)).strftime("%Y-%m-%d"), datetime.now().strftime("%Y-%m-%d"))
        #sql_string = "SELECT %s FROM %s where date_trunc('day', datestamp) = '%s' ORDER BY guid;" \
        #                     % (", ".join(self.__fields), table_name, datetime.now().strftime("%Y-%m-%d"))
        data = self.__dbManager.fetch(sql_string)
        for row in data:
            guid = row[0]
            if guid not in EXISTING_ADVERTS:
                EXISTING_ADVERTS.append(row[0])

    def save_db(self, table_name, result):
        success = 0
        failure = 0
        for item in result:
            fields = item.getFields()
            values = [item.getParam(field) for field in fields]
            try:
                sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                    table = table_name,
                    keys = ",".join(fields),
                    values = ", ".join(["%s" for i in fields])
                )
                self.__dbManager.fetch(sql_string, values, returnData=False)
                success += 1
            except:
                print("Unable to add this ad - already exists")
                failure += 1
        print("Data import to DB completed. Inserted items: %d. Failed items: %d" % (success, failure))

    def put_task(self):
        global table_name
        town = "moskva"
        self.__result = dict()
        page_template = "https://m.avito.ru/" + town + "/kvartiry/sdam?user=1&page=%d"
        for page_no in xrange(settings.START_PAGE, settings.MAX_PAGE_COUNT):
            self.__threadManager.put_task(page_template % page_no)
        result = self.__threadManager.wait_lock()
        print("<----------- Got result ------------>")
        print("Total amount of items: %d" % len(result))
        self.save_db(table_name, result)



# -----------------------------------------------------------

class Scheduler(threading.Thread):
    def __init__(self, kernel, event):
        threading.Thread.__init__(self)
        self.__stopEvent = event
        self.__kernel = kernel
        today = datetime.now()
        self.__schedList = {
            "update": today + timedelta(minutes=0)
        }

    def run(self):
        while not self.__stopEvent.wait(5):
            today = datetime.now()
            events = self.__kernel._events()
            for action, stamp in self.__schedList.items():
                if today > stamp:
                    print("Action: %s. Datetime: %s.Now is: %s" % (action, stamp, today))
                    print(self.__schedList)
                    self.__schedList[action] += timedelta(seconds=20)
                    # thread.start_new_thread(events[action], ())
                    events[action]()
                    print("-------------------- >>>>>>>>>>>>>>>> Schedule completed")

# -----------------------------------------------------------

def main():
    global RULES_SYSTEM
    config = ConfigManager()
    dbmanager = DBManager(driver = "PG")
    dbmanager.setCreds("host=127.0.0.1 dbname=avitoparser user=postgres password=acuario")
    RULES_SYSTEM = config.getConfig("WebCrawler")
    # rs = RuleSystem()
    # rs.load(**config.getConfig("WebCrawler"))
    evt = Event()
    parser = WebParser(dbmanager)
    parser.load_db()
    # parser.setRules(**config.getConfig("WebCrawler"))
    shed = Scheduler(parser, evt)
    shed.start()
    # parser.put_task()

if __name__ == '__main__':
    main()
