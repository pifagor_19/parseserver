# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import urllib2, traceback, time
from Queue import Queue, Empty
from threading import Lock, Thread, Event
from bs4 import BeautifulSoup
from datetime import datetime, timedelta

location = "moskva"#"lyubertsy" #
page_template = "https://m.avito.ru/%s/kvartiry/sdam?user=1&page=%d"

START_PAGE = 0
STOP_PAGE  = 5
PAGE_NUMBER = 0
THREAD_COUNT = 25
CONN_TIMEOUT = 20
REFRESH_INTERVAL = 20


###################################################
# SSL PATCH                                       #
###################################################

# import ssl
# from functools import wraps
# def sslwrap(func):
#     @wraps(func)
#     def bar(*args, **kw):
#         kw['ssl_version'] = ssl.PROTOCOL_TLSv1
#         return func(*args, **kw)
#     return bar
#
# ssl.wrap_socket = sslwrap(ssl.wrap_socket)

###################################################

str_to_dt = lambda f: datetime(f, "%Y %d %b %H:%M")

class ParserQueue(Queue):
    def _init(self, maxsize):
        Queue._init(self, maxsize)
        self.all_items = set()

    def _put(self, item):
        if item not in self.all_items:
            Queue._put(self, item)
            self.all_items.add(item)

    def _get(self):
        Queue._get(self)
        return self.all_items.pop()

def _modHeader(responce, urllink):
    global HEADERS
    HEADERS["Referer"] = urllink
    cookie = ""
    if responce:
        cookie_list = ["sessid=", "u=", "v="]
        for it in responce.info().headers:
            for i in cookie_list:
                if it.find(i) != -1:
                    cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
    HEADERS["Cookie"] = cookie

class WebCrawler(object):
    def __init__(self):
        self.__pool = ThreadPool(THREAD_COUNT)
        self.__headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
        }
        self.__visitedUrls = list()
        self.__event = Event()
        self.__scheduler = Scheduler(self, self.__event)
        self.__scheduler.start()

    def _events(self):
        return {"update": self.put_task}

    def put_task(self):
        page_template = "https://m.avito.ru/" + "lyubertsy" + "/kvartiry/sdam?user=1&page=%d"
        for page in xrange(START_PAGE, STOP_PAGE):
            self.__pool.add_task(self.parse_search, page_template % page)
        self.__pool.wait_completion()
        print("Execution completed")

    def get_url(self, url):
        base_url = "https://m.avito.ru"
        url_to_visit = base_url + url.get("href")
        _result = self._open(url_to_visit)
        #req = urllib2.Request(url_to_visit, None, self.__headers)
        #response = urllib2.urlopen(req)
        parser = BeautifulSoup(_result)
        div = parser.find('a', 'person-action button button-green action-show-number action-link link ')
        ph = div.get('href')
        return div

    def _modHeader(self, responce, urllink):
        self.__headers["Referer"] = urllink
        cookie = ""
        if responce:
            cookie_list = ["sessid=", "u=", "v="]
            for it in responce.info().headers:
                for i in cookie_list:
                    if it.find(i) != -1:
                        cookie += it.replace("Set-Cookie: ", "").split(";")[0] + ";"
        self.__headers["Cookie"] = cookie

    def _open(self, url_to_visit):
        try:
            # print("-----------------------------------------")
            print("Opening page %s" % url_to_visit)
            request = urllib2.Request(url_to_visit, None, self.__headers)
            t = time.time()
            responce =urllib2.urlopen(request)
            print("Timeout is: %d" % (time.time() - t))
            self._modHeader(responce, url_to_visit)
            return responce.read()
        except:
            print("Exception on search page %s. Details: %s" % (url_to_visit, traceback.format_exc()))
            raise Exception

    def parse_search(self, search_url):
        try:
            base_url = "https://m.avito.ru"
            print(search_url)
            _result = self._open(search_url)
            if not _result:
                raise Exception
            parser = BeautifulSoup(_result)
            tags = parser.find_all("article")
            for item in tags:
                url = base_url + item.a.get("href")
                datetamp = item.find("div", "info-date info-text").text.replace("\n", "").replace(",", "")
                # print(url)
                # print(datetamp)
                if len(url.split("._")) == 2 and url not in self.__visitedUrls:
                    self.__pool.add_task(self.parse_advert, url)
        except Exception:
            print("Exception on search page %s. Details: %s" % (search_url, traceback.format_exc()))
            time.sleep(2)
            self.__pool.add_task(self.parse_search, search_url)

    def parse_advert(self, advert_url):
        print("Parse advert: %s" % advert_url)
        try:
            _result = self._open(advert_url)
            if not _result:
                raise Exception
            parser = BeautifulSoup(_result)
            tags = parser.find("title")
            desc = tags.text.split(' - ')[0].encode('utf-8').replace('\n', '')
            print(desc)
            phoneDiv = parser.find(*["a", "person-action button button-blue action-show-number action-link link "])
            phone = self.get_url(phoneDiv)
            print(phone.get('href').replace('tel:', '').replace('-','').replace(' ', ''))
            if advert_url not in self.__visitedUrls:
                print(len(self.__visitedUrls))
                self.__visitedUrls.append(advert_url)
        except Exception:
            print("Exception on advert page %s. Details: %s" % (advert_url, traceback.format_exc()))
            time.sleep(2)
            self.__pool.add_task(self.parse_advert, advert_url)

class Worker(Thread):
    """Thread executing tasks from a given tasks queue"""
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try: func(*args, **kargs)
            except Exception, e: print e
            self.tasks.task_done()

class ThreadPool:
    """Pool of threads consuming tasks from a queue"""
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads): Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """Add a task to the queue"""
        self.tasks.put((func, args, kargs))

    def wait_completion(self):
        """Wait for completion of all the tasks in the queue"""
        self.tasks.join()

import threading, thread

class Scheduler(threading.Thread):
    def __init__(self, kernel, event):
        threading.Thread.__init__(self)
        self.__stopEvent = event
        self.__kernel = kernel
        today = datetime.now()
        self.__schedList = {
            "update": today + timedelta(minutes=0)
        }

    def run(self):
        while not self.__stopEvent.wait(5):
            today = datetime.now()
            events = self.__kernel._events()
            for action, stamp in self.__schedList.items():
                if today > stamp:
                    print("Action: %s. Datetime: %s.Now is: %s" % (action, stamp, today))
                    print(self.__schedList)
                    self.__schedList[action] += timedelta(seconds=20)
                    thread.start_new_thread(events[action], ())
                    #events[action]()
                    print("-------------------- >>>>>>>>>>>>>>>> Schedule completed")

def main():
    try:
        global PAGE_NUMBER

        visited_urls = list()
        search_page_queue = ParserQueue()#Queue()
        advert_page_queue = ParserQueue()#Queue()
        exit_event = Event()
        base_url = "https://m.avito.ru"

        def get_date(datestamp):
            datestamp = datestamp.lower().replace("\\xc2\\xa", "")
            today = datetime.now()
            rep_d = {
                "сегодня": today.strftime("%d %b"),
                "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
                "авг.": "Aug",
                "сен.": "Sep",
                "Размещено ": "",
                "в ": ""
            }
            result = datestamp.encode("utf-8")
            for k,v in rep_d.items():
                result = result.replace(k, v)
            d = today.strftime("%Y") + ' ' + result
            return datetime.strptime(d, "%Y %d %b %H:%M")

        def get_url(url):
            url_to_visit = base_url + url.get("href")
            req = urllib2.Request(url_to_visit, None, HEADERS)
            response = urllib2.urlopen(req)
            parser = BeautifulSoup(response.read())
            div = parser.find(['a', 'person-action button button-green action-show-number action-link link '])
            ph = div.get('href')
            return div

        def _open(url_to_visit):
            try:
                # print("-----------------------------------------")
                print("Opening page %s" % url_to_visit)
                request = urllib2.Request(url_to_visit, None, HEADERS)
                t = time.time()
                responce =urllib2.urlopen(request, timeout=CONN_TIMEOUT)
                print("Timeout is: %d" % (time.time() - t))
                _modHeader(responce, url_to_visit)
                if url_to_visit not in visited_urls:
                    print(len(visited_urls))
                    visited_urls.append(url_to_visit)
                return responce
            except:
                print("Exception on search page %s. Details: %s" % (url_to_visit, traceback.format_exc()))
                raise Exception

        def search_page():
            while not exit_event.isSet():
                try:
                    page = search_page_queue.get()
                    #search_page_queue.task_done()
                    print(page)
                    _result = _open(page)
                    if not _result:
                        raise Exception
                    parser = BeautifulSoup(_result.read())
                    tags = parser.find_all("article")
                    for item in tags:
                        url = base_url + item.a.get("href")
                        datetamp = item.find("div", "info-date info-text").text.replace("\n", "").replace(",", "")
                        if len(url.split("._")) == 2 \
                                and url not in visited_urls:
                            advert_page_queue.put(url)
                        #print("%s --> %s" % (url, datetamp))
                    #advert_page_queue.task_done()
                except Empty:
                    pass
                except Exception:
                    print("Exception on search page %s. Details: %s" % (page, traceback.format_exc()))
                    time.sleep(2)
                    search_page_queue.put(page)

        def advert_page():
            while not exit_event.isSet():
                try:
                    page = advert_page_queue.get()
                    #advert_page_queue.task_done()
                    print(page)
                    _result = _open(page)
                    if not _result:
                        raise Exception
                    parser = BeautifulSoup(_result.read())
                    tags = parser.find("title")
                    desc = tags.text.split(' - ')[0].encode('utf-8').replace('\n', '')
                    print(desc)
                    # phoneDiv = parser.find(*["a", "person-action button button-blue action-show-number action-link link "])
                    # phone = get_url(phoneDiv)
                    # print(phone.get('href').replace('tel:', '').replace('-','').replace(' ', ''))

                except Empty:
                    print("<<<<<<<<<<< ---------- Empty exception  ----------  >>>>>>>>>>>>")
                except Exception:
                    print("Exception on advert page %s. Details: %s" % (page, traceback.format_exc()))
                    time.sleep(2)
                    advert_page_queue.put(page)

        def put_task():
            while not exit_event.isSet():
                print("<<<<<<<<<<< ---------- Empty  ----------  >>>>>>>>>>>>")
                for loc in [ "moskva","lyubertsy"]:
                    for page_number in xrange(START_PAGE, STOP_PAGE, 1):
                        webpage = page_template % (loc, page_number)
                        search_page_queue.put(webpage)
                search_page_queue.task_done()
                time.sleep(REFRESH_INTERVAL)

        threads = [Thread(target=put_task)]
        threads.append(Thread(target=search_page))
        # threads = [Thread(target=search_page)]
        [threads.append(Thread(target=advert_page)) for i in xrange(THREAD_COUNT)]

        # put_task()

        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        print("Task completed")
        # search_page_queue.join()
        # advert_page_queue.join()
    except:
        print(traceback.format_exc())

def main2():
    web = WebCrawler()
    web.put_task()
    # while True:
    #     web.put_task()
    #     time.sleep(REFRESH_INTERVAL)

if __name__ == "__main__":
    #main()
    main2()