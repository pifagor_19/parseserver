# -*- coding: utf-8 -*-
import smtplib
from email.mime.text import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import smtplib, os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import formatdate
from email import Encoders

def sendGmail(fromaddr, toaddr, username, password, email_body, email_subject, files = []):
    msg = MIMEMultipart()
    msg['Subject'] = email_subject
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Date'] = formatdate(localtime=True)
    msg.attach( MIMEText(email_body.encode("utf-8"), _charset='utf-8') )
    try:
        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload( open(f,"rb").read() )
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part)

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(username,password)
        server.sendmail(fromaddr, toaddr, msg.as_string())
        server.quit()
    except Exception as e:
        print "Something went wrong when sending the email %s" % fromaddr
        print e

email_template = {
    "body": u'''    Здравствуйте! Вы получили это письмо, потому что зарегистрированы
на сервисе оповещения о новых рекалмных сообщениях. Не отвечайте на это
письмо - оно автоматически сгенерировано ботом.

    Hello! You received this message, because you have subscribed to notification
on new adverts in site avito.ru. Do not respond back to this message - it is
automatically generated.''',
    "subject": "Рассылка уведомлений по почте"
}

sendGmail("infoservicebot@gmail.com",
          "topifagorsend@gmail.com",
          "infoservicebot@gmail.com",
          "086221ssau",
          email_template["body"],
          email_template["subject"],
          ["Report_2014_08_22.xlsx"])