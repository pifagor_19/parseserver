# -*- coding: utf-8 -*-
import urllib2, traceback, cookielib, os
from bs4 import BeautifulSoup

__author__ = 'admin'

http_link = "https://m.avito.ru/lyubertsy/komnaty/komnata_12_m_v_3-k_110_et._392607499"

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    #"Accept-Encoding": "gzip,deflate",
    #"Accept-Charset": "UTF-8",
    #"Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
    #"Cache-Control": "no-cache",
    #"Cookie": "",
    #"Connection": "keep-alive",
    #"Host": "m.avito.ru",
    #"Pragma": "no-cache",
    #"Referer": "https://m.avito.ru/lyubertsy/komnaty/",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
}

#"phone": {
#    "rule": "lambda f: get_url(f).get('href').replace('tel:', '').replace('-','').replace(' ', '')",
#    "tags": ["a",
#    "person-action button button-blue action-show-number action-link link "]
#}

os.system("cls")
print("------------------------------------------------------------------------\n")
print("                    REQUEST 1")
print("------------------------------------------------------------------------\n")
req = urllib2.Request(http_link, None, headers)
#req = urllib2.Request(http_link)
# for item, value in req.headers.items():
#     print("%s: %s\n" % (item, value))
resp = urllib2.urlopen(req)

print("\n------------------------------------------------------------------------\n")
print("                    RESPONCE 1")
print("------------------------------------------------------------------------\n")

newcookies = ""
cookie_list = ["sessid=", "u=", "v="]

for item in resp.info().headers:
    for i in cookie_list:
        if item.find(i) != -1:
            l = item.replace("Set-Cookie: ", "").split(";")
            newcookies += l[0] + ";"

data = resp.read()

headers["Cookie"] = "" + newcookies
headers["Referer"] = "https://m.avito.ru/lyubertsy/komnaty/komnata_12_m_v_3-k_110_et._392607499"

soup = BeautifulSoup(data)
base_http = "https://m.avito.ru"
query = base_http + soup.find("a", "person-action button button-blue action-show-number action-link link ").get("href")# + "?async"
print(query)
req = urllib2.Request(query, None, headers)
print("\n------------------------------------------------------------------------\n")
print("                    REQUEST 2")
print("------------------------------------------------------------------------\n")
for item, value in req.headers.items:
    print("%s: %s\n" % (item, value))
resp = urllib2.urlopen(req)

print("\n------------------------------------------------------------------------\n")
print("                    RESPONCE 2")
print("------------------------------------------------------------------------\n")
for item in resp.info().headers:
    print(item)
data = resp.read()
soup = BeautifulSoup(data)
print("\n------------------------------------------------------------------------\n")
print("                    PHONE NUMBER")
print("------------------------------------------------------------------------\n")
print("Phone: ", soup.find("a", "person-action button button-green action-show-number action-link link "))
      #.get("href")
      #.replace("tel:", "").replace("-","").replace(" ", ""))
