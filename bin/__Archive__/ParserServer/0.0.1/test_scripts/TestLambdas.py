__author__ = 'admin'
d = [
    {
        "html": "Hello - world",
        "lambda": "lambda g: g.split(' - ')[0]"
    },
    {
        "html": "Hello1 : world1",
        "lambda": '''lambda g: g.split(' : ')[1]'''
    }
]
print(d)
for rule in d:
    r = eval(rule["lambda"])(rule["html"])
    print(r)