# -*- coding: utf-8 -*-
import psycopg2, traceback, re

__author__ = 'admin'

kvartiry = dict()

fields = ["guid", "url", "category", "town", "price", "phone", "contact", "address", "description", "title", "datestamp"]

def load():
    #c_str = "host=127.0.0.1 dbname=avitoparser_old user=postgres password=toor"
    c_str = "host=127.0.0.1 dbname=avitoparser user=postgres password=acuario"
    connect = None
    try:
        connect = psycopg2.connect(c_str)
    except:
        print(traceback.format_exc())
        return
    cursor = connect.cursor()
    query = "SELECT %s FROM adverts where date_trunc('day', datestamp) = '2014-09-01' ORDER BY guid;" % ", ".join(fields)
    print(query)
    cursor.execute(query)
    results = cursor.fetchall()
    for item in results:
        advert = dict(zip(fields, list(item)))
        guid = advert["guid"]
        #kvartiry.append(advert)
        if guid not in kvartiry:
            kvartiry[guid] = advert
        else:
            print(guid)

def select(criteria):
    ret_res = {}
    true_case = 0
    false_case = 0
    for guid, advert in kvartiry.items():
        res = eval(criteria)(advert)
        ret_res[guid] = res
        if res:
            true_case += 1
        else:
            false_case += 1
            print(guid)
        #print(guid, res)
    print("Fake adverts: %d, Real adverts: %d, Total count: %d" % (true_case, false_case, len(kvartiry)))
    return ret_res

import itertools

rep_dict = {
        u"а" : "a",
        u"е" : "e",
        u"о" : "o",
        u"у" : "y",
        u"р" : "p",
        u"к" : "k",
        u"х" : "x",
        u"с" : "c"
    }

def gen_query(input_dict):
    output = list()
    for word, r_dict in input_dict.items:
        for item in itertools.product([0,1], repeat = len(r_dict)):
            m_word = list(word)
            ctr = 0
            for i in item:
                if i: m_word[r_dict.keys()[ctr]] = rep_dict[m_word[r_dict.keys()[ctr]]]
                ctr += 1
            output.append("".join(m_word))
    return output

def variation(input):
    ret_val = []
    words_rep = dict()
    for item in input:
        pos = 0
        r_dict = dict()
        for letter in item:
            if letter in rep_dict.keys():
                r_dict[pos] = letter
                #print(letter)
            pos += 1
        if r_dict:
            words_rep[item] = r_dict
    #print(words_rep)
    return gen_query(words_rep)

import xlsxwriter, os
from datetime import datetime
today = datetime.today()
param_keys = ["guid", "datestamp", "url", "category", "town", "price", "contact", "phone", "address", "title", "description"]

def xlsx_exporter():
    global format_guid
    print(today)
    params_keys = param_keys
    # file_desc = open("./reports/kvartiry_ext.json", "r").read()
    # kvartiry = json.loads(file_desc)
    print("Report Len: ", len(kvartiry))
    #rep_name = "Report_" + today.strftime("%Y_%m_%d-%H_%M") + ".xlsx"
    rep_name = "Report_" + today.strftime("%Y_%m_%d") + ".xlsx"
    if os.path.isfile(rep_name):
        os.remove(rep_name)
    report = xlsxwriter.Workbook(rep_name)
    bold = report.add_format({'bold': 2})
    wrong_guid = report.add_format({'bg_color': '#E66761'})
    correct_guid = report.add_format({'bg_color': '#90EE90'})
    worksheet = report.add_worksheet()
    column_count = 0
    for item in params_keys:
        worksheet.write(0, column_count, item, bold)
        worksheet.set_column(0, column_count, 15)
        column_count += 1
    row_count = 1
    for advert in kvartiry.values():
        column_count = 0
        guid = advert["guid"]
        d = filtered_ads[guid]
        format_guid = wrong_guid if d else correct_guid
        for item in params_keys:
            if item == "url":
                worksheet.write_url(row_count, column_count, advert[item])
            elif item == "datestamp":
                worksheet.write_string(row_count, column_count, advert[item].strftime("%d %B %Y %H:%M"))
            else:
                if item != "guid":
                    format_guid = None
                try:
                    worksheet.write_string(row_count, column_count, advert[item].decode("utf-8"), format_guid)#.decode("utf-8")
                except:
                    worksheet.write_string(row_count, column_count, unicode(advert[item]), format_guid)
            column_count += 1
        row_count += 1
    worksheet.autofilter(0, 0, len(kvartiry)-1, len(param_keys)-1)
    report.close()

filtered_ads = {}

if __name__ == "__main__":
    global filtered_ads
    load()
    #filter_seq = ['(%s.+?)' % i for i in variation([u"тел", u"фото", u"номер", u"агент"])]
    filter_seq = ['(%s.+?)' % i for i in variation([u"фото", u"номер"])]
    #filter_seq.append(u'тел\\W')
    filter_seq.append(u'/телеф\\W')
    #select("lambda f: len(re.findall('(тел.+?)|(тeл.+?)|(фото.+?)|(фoто.+?)|(фoтo.+?)|(фотo.+?)|(номер.+?)|(нoмер.+?)|(нoмeр.+?)|(номeр.+?)', f['description']))")
    print("lambda f: len(re.findall('%s', f['description']))" % "|".join(filter_seq))
    #filtered_ads = select("lambda f: len(re.findall('%s', f['description'])) and len(f['description']) > 25 and not len(re.findall('^8999\\d+', f['description']))" % "|".join(filter_seq))
    filtered_ads = select("lambda f: len(re.findall('^8999', f['phone'])) > 0 or len(re.findall('%s', f['description'])) > 0 or len(f['description']) < 35" % "|".join(filter_seq))
    xlsx_exporter()
