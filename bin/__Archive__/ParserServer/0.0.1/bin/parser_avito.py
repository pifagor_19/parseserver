# -*- coding: utf-8 -*-
from pydoc import source_synopsis
from bs4 import BeautifulSoup
import urllib2, re, json, traceback
import cookielib
import datetime
from datetime import datetime
from datetime import timedelta
import xlsxwriter
import smtplib
from email.mime.text import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import smtplib, os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import formatdate
from email import Encoders
import psycopg2

today = datetime.today()
cookies = cookielib.CookieJar()
handlers = [
    urllib2.HTTPHandler(),
    urllib2.HTTPSHandler(),
    urllib2.HTTPCookieProcessor(cookies),
    #urllib2.ProxyHandler({"http":"http://190.25.157.30:8080"})
    ]
opener = urllib2.build_opener(*handlers)

class Advert:
    def __init__(self):
        self.__guid = ""
        self.__url = ""

    def to_json(self):
        return {"guid": self.__guid, "url": self.__url}

newheaders = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    #"Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
    #"Cache-Control": "no-cache",
    #"Cookie": "" + newcookies,
    #"Connection": "keep-alive",
    #"Host": "m.avito.ru",
    #"Pragma": "no-cache",
    #"Referer": "https://m.avito.ru/lyubertsy/kvartiry/1-k_kvartira_35_m_510_et._391152420",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.8 Safari/537.36"
}

class IConverter(object):
    def __init__(self):
        self.__converter = {u"янв.": 1,
                            u"фев.": 2,
                            u"мар.": 3,
                            u"апр.": 4,
                            u"мая": 5,
                            u"июня": 6,
                            u"июля": 7,
                            u"авг.": 8,
                            u"сен.": 9,
                            u"окт.": 10,
                            u"ноя.": 11,
                            u"дек.": 12,
        }

class AdvertSerializer(json.JSONEncoder):
    def default(self, o):
        return o.to_json()

base_http = "https://m.avito.ru"
room_list = list()
kvartiry = list()

def mod_header(resp, url):
    global newheaders
    newheaders["Referer"] = url
    newcookies = ""
    if not resp:
        newheaders["Cookie"] = newcookies
        return
    cookie_list = ["sessid=", "u=", "v="]
    for it in resp.info().headers:
        for i in cookie_list:
            if it.find(i) != -1:
                l = it.replace("Set-Cookie: ", "").split(";")
                newcookies += l[0] + ";"
    newheaders["Cookie"] = newcookies

def web_crawler():
    global newheaders
    regexp_pattern = "._"
    location = "lyubertsy"
    #category = "nedvizhimost"
    category = "kvartiry"
    ad_type = "sdam"
    sort_filter = "user=1&page=%d"
    http_link = base_http + "/" + location + "/" + category + "/" + ad_type + "?" + sort_filter
    #http_link = "http://m.avito.ru/lyubertsy/nedvizhimost?user=1"
    print("Parsing html pages")
    page_increment = 0
    try:
        while True:
            if page_increment == 10:
                raise Exception
            req = urllib2.Request(http_link % page_increment, None, newheaders)

            print(http_link % page_increment)
            #print("Headers: ", req.headers)
            response = urllib2.urlopen(req)
            # print(response.headers)
            mod_header(response, http_link % page_increment)
            the_page = response.read()
            soup = BeautifulSoup(the_page)
            for link in soup.find_all("a"):
                urllink = link.get("href")
                sp = urllink.split(regexp_pattern)
                if len(sp) == 2:
                    guid = sp[1]
                    url = str(base_http + urllink)
                    if url not in room_list:
                        room_list.append(url)
            page_increment += 1
    except:
        print(traceback.format_exc())
        print("All elements found. Stop at %d page" % page_increment)

    # print("Writing to file")
    # with open("kvartiry.json", "w") as outfile:
    #     json.dump(room_list, outfile)

def get_infotag(htmlTree, container, classID):
    info = ""
    try:
        info = htmlTree.find(container, classID).text
    except Exception as e:
        print(traceback.format_exc())
        return ""
    return info.encode("utf-8").replace("\n", "")

f_rules = open("./rules/avito.rules", "r")
search_params = json.load(f_rules)["content"]
param_keys = ["guid", "datestamp", "url", "category", "town", "price", "contact", "phone", "address", "title", "description"]

def get_url(url):
    global newheaders
    #print(url.get("href"))
    req = urllib2.Request(base_http + url.get("href"), None, newheaders)
    #print("Headers: ", req.headers)
    response = urllib2.urlopen(req)
    #print("RESPPONCE: ", response.info().headers)
    data = response.read()
    #print(data)
    soup = BeautifulSoup(data)
    return soup.find('a', 'person-action button button-green action-show-number action-link link ')
    #print("Phone: ", soup.find("a", "person-action button button-green action-show-number action-link link "))
    print("Phone: ", soup.find('a', 'person-action button button-green action-show-number action-link link ').get('href')
       .replace('tel:', '').replace('-','').replace(' ', ''))

def replace(input_str, **vals):
    result = input_str.encode("utf-8")
    for k,v in vals.items():
        result = result.replace(k, v)
    return result

def get_date(datestamp):
    rep_d = {
        "сегодня": today.strftime("%d %b"),
        "вчера": (today - timedelta(days = 1)).strftime("%d %b"),
        "авг.": "Aug",
        "Размещено ": "",
        "в ": ""
    }
    d = today.strftime("%Y")+' '+replace(datestamp, **rep_d)
    return datetime.strptime(d, "%Y %d %b %H:%M")

import time

def reconnect():
    os.system("poff 3g")
    time.sleep(10)
    os.system("pon 3g")
    time.sleep(10)

def info_revealer():
    global newheaders
    counter = 0
    #file_desc = open("kvartiry.json", "r").read()
    #kvartiry = json.loads(file_desc)
    # room_list = ["https://m.avito.ru/lyubertsy/komnaty/komnata_12_m_v_3-k_110_et._392607499"]

    def page_fetch(page_url, counter):
        try:
            mod_header(None, page_url)
            req = urllib2.Request(page_url, None, newheaders)
            print("%d) %s" % (counter, page_url))
            response = urllib2.urlopen(req)
            the_page = response.read()
            mod_header(response, page_url)
            soup = BeautifulSoup(the_page)
            advert = {"url": page_url}
            for rule_name, rule in search_params.items:
                result = eval(rule["rule"])(soup.find(*rule["tags"]))
                advert[rule_name] = result
            url_tag = get_url(soup.find("a", "person-action button button-blue action-show-number action-link link "))
            advert["phone"] = url_tag.get('href').replace('tel:', '').replace('-','').replace(' ', '')
            datestamp = get_date(soup.find("div", "item-add-date").text)
            advert["datestamp"] = datestamp
            kvartiry.append(advert)
        except:
            print("BANNED. RECONNECTING.....")
            mod_header(None, None)
            print(traceback.format_exc())
            print("Unable to download page")
            reconnect()
            page_fetch(page_url, counter)

    for item in room_list:
        page_fetch(item, counter)
        counter += 1
    # with open("./reports/kvartiry_ext.json", "w") as outfile:
    #     json.dump(kvartiry, outfile, ensure_ascii=False)

conn_creds = "hostaddr=127.0.0.1 port=5432 dbname=avitoparser user=postgres password=acuario"

def db_save():
    # file_desc = open("kvartiry_ext.json", "r").read()
    # kvartiry = json.loads(file_desc)
    connection = None
    try:
        connection = psycopg2.connect(conn_creds)
        connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    except:
        print(traceback.format_exc())

    cursor = connection.cursor()
    for item in kvartiry:
        try:
            sql_string = "INSERT INTO {table}({keys}) VALUES({values})".format(
                table = "adverts",
                keys = ",".join(item.keys()),
                values = ", ".join(["%s" for i in item.keys()])
            )
            cursor.execute(sql_string, item.values())
        except:
            print("Unable to add this ad - already exists")
    connection.close()

import os

def xlsx_exporter():
    print(today)
    params_keys = param_keys
    # file_desc = open("./reports/kvartiry_ext.json", "r").read()
    # kvartiry = json.loads(file_desc)
    print("Report Len: ", len(kvartiry))
    #rep_name = "Report_" + today.strftime("%Y_%m_%d-%H_%M") + ".xlsx"
    rep_name = "Report_" + today.strftime("%Y_%m_%d") + ".xlsx"
    if os.path.isfile(rep_name):
        os.remove(rep_name)
    report = xlsxwriter.Workbook("./reports/" + rep_name)
    bold = report.add_format({'bold': 2})
    worksheet = report.add_worksheet()
    column_count = 0
    for item in params_keys:
        worksheet.write(0, column_count, item, bold)
        worksheet.set_column(0, column_count, 15)
        column_count += 1
    row_count = 1
    for advert in kvartiry:
        column_count = 0
        for item in params_keys:
            if item == "url":
                worksheet.write_url(row_count, column_count, advert[item])
            elif item == "datestamp":
                worksheet.write_string(row_count, column_count, advert[item].strftime("%d %B %Y %H:%M"))
            else:
                try:
                    worksheet.write_string(row_count, column_count, advert[item].decode("utf-8"))#.decode("utf-8")
                except:
                    worksheet.write_string(row_count, column_count, unicode(advert[item]))
            column_count += 1
        row_count += 1
    worksheet.autofilter(0, 0, len(kvartiry)-1, len(param_keys)-1)
    report.close()

def sendGmail(fromaddr, toaddr, username, password, email_body, email_subject, files = []):
    msg = MIMEMultipart()
    msg['Subject'] = email_subject
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Date'] = formatdate(localtime=True)
    msg.attach( MIMEText(email_body, _charset='utf-8') )
    try:
        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload( open(f,"rb").read() )
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part)

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(username,password)
        server.sendmail(fromaddr, toaddr, msg.as_string())
        server.quit()
    except Exception as e:
        print "Something went wrong when sending the email %s" % fromaddr
        print e

def send_report():
    email_template = {
        "body": "    Здравствуйте! Вы получили это письмо, потому что зарегистрированы " +
            "на сервисе оповещения о новых рекламных сообщениях. Не отвечайте на это " +
            "письмо - оно автоматически сгенерировано ботом." + "\n\n" +
            "    Hello! You received this message, because you have subscribed to notification " +
            "on new adcerts in site avito.ru. Do not respond back to this message - it is " +
            "automatically generated.".encode("utf-8"),
        "subject": u"Рассылка уведомлений по почте".encode("utf-8")
    }
    rep_name = "Report_" + today.strftime("%Y_%m_%d") + ".xlsx"
    sendGmail("infoservicebot@gmail.com",
              "savinkovauv@gmail.com",
              "infoservicebot@gmail.com",
              "086221ssau",
              email_template["body"],
              email_template["subject"],
              ["./reports/" + rep_name])

file_desc = open("./filters/stop.filters", "r").read()
deny_list = json.loads(file_desc)

def filter():
    file_desc = open("kvartiry_ext.json", "r").read()
    kvartiry = json.loads(file_desc)

if __name__ == "__main__":
    web_crawler()
    info_revealer()
    # file_desc = open("./reports/kvartiry_ext.json", "r").read()
    # kvartiry = json.loads(file_desc)
    # for advert in kvartiry:
    #     v = advert["description"]
    #     latin = re.findall(u"[\0128-\u0255]+", v)
    #     print(latin)
        # for k,v in advert.items():
        #     latin = re.findall(u"[\0400-\u0500]+", v)
        #     print(latin)
    #xlsx_exporter()
    db_save()
    #send_report()
