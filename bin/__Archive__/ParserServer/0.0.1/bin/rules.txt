{
	"category": {
		"rule": "lambda f: f.encode('utf-8').replace('\\n', '')",
		"tags": ["span",
		"param param-last"]
	},
	"town": {
		"rule": "lambda f: f.encode('utf-8').replace('\\n', '')",
		"tags": ["span",
		"info-text"]
	},
	"contact": {
		"rule": "lambda f: f.encode('utf-8').replace('\\n', '')",
		"tags": ["div",
		"person-name"]
	},
	"description": {
		"rule": "lambda f: f.encode('utf-8').replace('\\n', '')",
		"tags": ["div",
		"description-wrapper"]
	},
	"address": {
		"rule": "lambda f: f.encode('utf-8').replace('\\n', '')",
		"tags": ["span",
		"info-text text-user-address"]
	},
	"guid": {
		"rule": "lambda f: re.search(r'\\d+', f).group().encode('utf-8')",
		"tags": ["div",
		"item-id"]
	},
	"price": {
		"rule": "lambda f: ''.join(re.findall(r'\\d+', f)).encode('utf-8')",
		"tags": ["span",
		"price-value"]
	},
	"title": {
		"rule": "lambda f: f.split(' - ')[0].encode('utf-8').replace('\\n', '')",
		"tags": ["title"]
	}
}