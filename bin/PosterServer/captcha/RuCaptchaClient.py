# -*- coding: utf-8 -*-
__author__ = 'Юлия'

import httplib, json
from decimal import Decimal

class RuCaptchaClient:
    api_key = "2ea31799206616f826d22fa8d8319f76"
    host = "http://rucaptcha.com/in.php"
    errors = {}

    def __init__(self):
        errors = self.errors
        errors.update({"CAPCHA_NOT_READY": "Капча в работе, ещё не расшифрована, необходимо повтороить запрос через несколько секунд."})
        errors.update({"ERROR_WRONG_ID_FORMAT": "Неверный формат ID капчи. ID должен содержать только цифры."})
        errors.update({"ERROR_WRONG_CAPTCHA_ID": "Неверное значение ID капчи."})
        errors.update({"ERROR_CAPTCHA_UNSOLVABLE": "Капчу не смогли разгадать 3 разных работника. Средства за эту капчу не списываются."})
        errors.update({"ERROR_WRONG_USER_KEY": "Не верный формат параметра key, должно быть 32 символа."})
        errors.update({"ERROR_KEY_DOES_NOT_EXIST": "Использован несуществующий key."})
        errors.update({"ERROR_ZERO_BALANCE": "Баланс Вашего аккаунта нулевой."})
        errors.update({"ERROR_NO_SLOT_AVAILABLE": "Текущая ставка распознования выше, чем максимально установленная в настройках Вашего аккаунта."})
        errors.update({"ERROR_ZERO_CAPTCHA_FILESIZE": "Размер капчи меньше 100 Байт."})
        errors.update({"ERROR_TOO_BIG_CAPTCHA_FILESIZE": "Размер капчи более 100 КБайт."})
        errors.update({"ERROR_WRONG_FILE_EXTENSION": "Ваша капча имеет неверное расширение, допустимые расширения jpg,jpeg,gif,png."})
        errors.update({"ERROR_IMAGE_TYPE_NOT_SUPPORTED": "Сервер не может определить тип файла капчи."})
        errors.update({"ERROR_IP_NOT_ALLOWED": "В Вашем аккаунте настроено ограничения по IP с которых можно делать запросы. И IP, с которого пришёл данный запрос не входит в список разрешённых."})

    def parseAnswer(self, serviceAnswer):
        if (self.errors.__contains__(serviceAnswer)):
            print("{0} ({1})".format(self.errors[serviceAnswer], serviceAnswer))
        else:
            if (serviceAnswer.StartsWith("OK|")):
                return serviceAnswer.Substring(3)
            else:
                return serviceAnswer

    def getCaptcha(self, captchaId):
        url = "{0}/res.php?key={1}&action=get&id={2}".format(self.host, self.api_key, captchaId)
        return self.makeGetRequest(url)

    def uploadCaptchaFile(self, fileName):
        return self.uploadCaptchaFile(fileName, None)

    def uploadCaptchaFile(self, fileName, config):
        with open(fileName, 'r') as input_file:
            params = {
                "key": self.api_key,
                "file":  input_file.read(),
                "phrase":   0,
                "regsense": 0,
                "numeric":  0,
                "calc":     0,
                "min_leng": 0,
                "max_len":  0,
                "language": 0
            }

        data = json.dumps(params)

        cnct = httplib.HTTPConnection(self.host, 80)


        cnct.request("POST", '/', data, {"Content-Type": "application/json"})

        response = cnct.getresponse()
        answer = self.parseAnswer(response.read())

        return answer

    def getBalance(self):
        url = "{0}/res.php?key={1}&action=getbalance".format(self.host, self.api_key)
        string_balance = self.makeGetRequest(url)

        balance = int(string_balance)
        return balance

    def makeGetRequest(self, url):
        request = httplib.HTTPConnection(url)
        request.Method = "GET"
        serviceAnswer = ""

        request.request("GET", url, "", "")
        response = request.getresponse()
        return self.parseAnswer(response.read())