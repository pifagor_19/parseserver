__author__ = 'Юлия'

from rest_framework import serializers

from ..model.author import Author

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author