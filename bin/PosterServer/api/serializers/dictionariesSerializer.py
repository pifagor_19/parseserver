__author__ = 'Юлия'

from rest_framework import serializers

from ..model.dictionaries import *

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location

class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type

class TermSerializer(serializers.ModelSerializer):
    class Meta:
        model = Term

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')