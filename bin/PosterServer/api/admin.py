__author__ = 'Юлия'

from django.contrib import admin
from .advert import *

admin.site.register(Advert)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Region)
admin.site.register(Location)
admin.site.register(Type)
admin.site.register(Term)
admin.site.register(Room)
admin.site.register(User)
