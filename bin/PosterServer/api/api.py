__author__ = 'Юлия'

from rest_framework import generics, permissions


from serializers.dictionariesSerializer import UserSerializer
from model.dictionaries import User

class UserList(generics.ListAPIView):
    model = User
    serializer_class = UserSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class UserDetail(generics.RetrieveAPIView):
    model = User
    serializer_class = UserSerializer
    lookup_field = 'username'