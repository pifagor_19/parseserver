__author__ = 'Юлия'

from django.db import models
from django.contrib.auth.models import AbstractUser

class Category(models.Model):
    name = models.CharField(max_length=63)

class Region(models.Model):
    name = models.CharField(max_length=63)

class Location(models.Model):
    name = models.CharField(max_length=63)

class Type(models.Model):
    name = models.CharField(max_length=63)

class Term(models.Model):
    name = models.CharField(max_length=63)

class Room(models.Model):
    name = models.SmallIntegerField()

class User(AbstractUser):
    description = models.CharField(max_length=63)