__author__ = 'Юлия'

from author import Author
from dictionaries import *

class Advert(models.Model):
    author = models.ForeignKey(Author, related_name='author_adverts')
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    price = models.IntegerField()
    region = models.ForeignKey(Region, related_name='region_adverts')
    location = models.ForeignKey(Location, related_name='location_adverts')
    category = models.ForeignKey(Category, related_name='category_adverts')
    type = models.ForeignKey(Type, related_name='type_adverts')
    term = models.ForeignKey(Term, related_name='term_adverts')
    room = models.ForeignKey(Room, related_name='room_adverts')
