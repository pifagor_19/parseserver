# -*- coding: utf-8 -*-
__author__ = 'admin'

import uuid
import time
from selenium import webdriver
from selenium.webdriver.support.select import Select


sample_advert_textBoxes = {
    "seller_name": "Максим",
    "email": "multinodus@gmail.com",
    "phone": "9171608016",
    "title": "NotebookQ45",
    "description": "NotebookQ45",
    "price": "35000"
}

sample_advert_selectBoxes = [
    ["region_id", "652560"],
    ["location_id", "653040"],
    ["category_id", "114"],
    ["param_716", "10201"],
    ["param_719", "10241"]
]

#voopoopoo

# interface class
class DataContainer(object):
    def __init__(self):
        self.__params = dict()

    def get(self, pName):
        if pName in self.__params.keys():
            return self.__params[pName]

    def set(self, pName, pValue):
        self.__params[pName] = pValue

# advert params aggregator
class AvitoAdvert(DataContainer):
    def __init__(self):
        super(AvitoAdvert, self).__init__()

# avito advert poster
class AvitoPoster(DataContainer):
    def __init__(self):
        super(AvitoPoster, self).__init__()

    def addAdvert(self, advert):
        guid = uuid.uuid4()
        DataContainer.set(self, guid, advert)

    def post(self):
        pass

def testSeleniumAndPhantomJS():
    inputTextFile = open("text.txt")
    inputHeaderFile = open("headers.txt")

    textAll = inputTextFile.read()
    headerAll = inputHeaderFile.read()

    sep = "-----------------------------"

    texts = textAll.split(sep)
    headers = headerAll.split(sep)

    inputTextFile.close()
    inputHeaderFile.close()

    for i in range(0, 200):
        sample_advert_textBoxes["description"] = texts[i]
        sample_advert_textBoxes["title"] = headers[i]



        # driver = webdriver.PhantomJS(executable_path="D:\phantomjs-1.9.7-windows\phantomjs.exe")
        #driver = webdriver.Firefox()
        driver = webdriver.Chrome("D:\chromeDriver\chromedriver.exe")
        driver.maximize_window()
        driver.get("https://m.avito.ru/add")


        for key, value in sample_advert_textBoxes.iteritems():
            print(key + " " + value)
            element = driver.find_element_by_name(key)
            print(element)
            element.send_keys(value.decode('utf-8'))


        for pair in sample_advert_selectBoxes:
            key = pair[0]
            value = pair[1]
            print(key + " " + value)
            element = driver.find_element_by_id(key)
            print(element)
            selectElement = Select(element)
            selectElement.select_by_value(value)
            time.sleep(8)

        driver.find_element_by_id("image_upload").send_keys("C:\\5YgaIFmpp2o")

        time.sleep(10)
        driver.find_element_by_class_name("control-self-submit").click()
        time.sleep(30)

        captchaURI = driver.find_element_by_xpath("//div[@class=\"captcha-image\"]/img").get_attribute("src")
        print(captchaURI)

        captchaClient = RuCaptchaClient()
        answer = captchaClient.uploadCaptchaFile(captchaURI, None)

        element = driver.find_element_by_name(key).send_keys(answer)

        driver.find_element_by_class_name("control-self-submit").click()
        time.sleep(10)


        driver.find_element_by_id("service_no").click()
        time.sleep(5)
        driver.find_element_by_class_name("control-self-submit").click()
        time.sleep(10)

        print driver.current_url
        driver.quit()

def main():
    # poster = AvitoPoster()
    # advert = AvitoAdvert()
    testSeleniumAndPhantomJS()


if __name__ == "__main__":
    main()