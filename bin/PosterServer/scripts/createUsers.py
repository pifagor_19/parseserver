__author__ = 'Юлия'

from ..api.model.dictionaries import User

users = ['Bob', 'Sally', 'Joe', 'Rachel']
for user in users:
    username = user.lower()
    User.objects.create(username=username, email="{}@example.com".format(username), first_name=user)