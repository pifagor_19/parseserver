-- Districts table

CREATE TABLE districts
(
    id integer,
    name text,
    description text,
    CONSTRAINT districts_pr_key PRIMARY KEY(id)
);

SELECT AddGeometryColumn('districts', 'cpoint', 4326, 'POINT', 2);

-- Places type table

CREATE TABLE types
(
    id integer,
    type_name text,
    CONSTRAINT types_pr_key PRIMARY KEY(id)
);

-- Places table with all objects

CREATE TABLE places
(
    id integer,
    loc_name text,
    street_name text NOT NULL,
    house_number text,
    obj_type integer NOT NULL DEFAULT 0,
    dist_type integer NOT NULL DEFAULT 0,
    CONSTRAINT places_pr_key PRIMARY KEY(id),
    CONSTRAINT places_type FOREIGN KEY (obj_type)
      REFERENCES types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT places_dist FOREIGN KEY (dist_type)
      REFERENCES districts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

SELECT AddGeometryColumn('places', 'way', 4326, 'GEOMETRY', 2);

-- Make indexation on fields

CREATE INDEX ON places using gist(way);
CREATE INDEX ON places(id);
CREATE INDEX ON places(loc_name);
CREATE INDEX ON places(house_number);
CREATE INDEX ON places(obj_type);
CREATE INDEX ON places(dist_type);

-- Fill in some basic data
-- Basic types

INSERT INTO types VALUES(0, 'unknown');
INSERT INTO types VALUES(1, 'building');
INSERT INTO types VALUES(2, 'school');
INSERT INTO types VALUES(3, 'suburb');

-- Main ditricts

INSERT INTO districts VALUES(0, 'unknown', 'Неизвестно', ST_SetSRID(ST_MakePoint(0, 0), 4326));
INSERT INTO districts VALUES(1, 'Moscow', 'Москва', ST_SetSRID(ST_MakePoint(55.751667, 37.617778), 4326));