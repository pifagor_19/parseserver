# -*- coding: utf-8 -*-
__author__ = 'adm1n'

import psycopg2, json, traceback
from threading import Thread
from Queue import Queue, Empty

DB_CREDS = "hostaddr=127.0.0.1 dbname=OSM_STREETS user=postgres password=acuario"
GEOJSON_INPUT_FILE = "msk.geojson"
CSV_INPUT_FILE = "msk.csv"
OBJECT = None
OBJECT_TYPE = {
    "suburb":   3,
    "school":   2,
    "building": 1,
    "yes":      1,
    "unknown":  0
}
REPORT_FILE = "streets.csv"

def parse_file():
    global OBJECT
    OBJECT = None
    f_stream = None
    # with open(GEOJSON_INPUT_FILE, "r") as f:
    #     f_stream = f.read()
    # collect(f_stream, "json")
    with open(CSV_INPUT_FILE, "r") as f:
        f_stream = f.read()
    collect(f_stream, "csv")

def csv_parse(input_csv):
    export_data = list()
    raw_data = [[item for item in rec.split("\t")] for rec in input_csv.split("\n") if rec]
    for record in raw_data:
        building = {}
        building["id"] = record[0]
        building["loc_name"] = record[1]
        building["way"] = "POINT(" + " ".join(record[2:]) + ")"
        building["obj_type"] = 3
        building["dist_type"] = 1
        export_data.append(building)
    print("Overall data size from CSV is: %d" % len(export_data))
    return export_data

def collect(input, format = 'json'):
    export_data = None
    if format == "json":
        export_data = json_parse(input)
    elif format == "csv":
        export_data = csv_parse(input)
    if export_data:
        db_export(export_data)

def json_parse(input_json):
    export_data = list()
    types = _types()
    input_json = json.loads(input_json)["features"]
    for item in input_json:
        try:
            building = {}
            geometry = item["geometry"]
            props = item["properties"]
            b_type = props["building"]
            building["obj_type"] = types[b_type] if b_type in types.keys() \
                else types["unknown"]
            building["dist_type"] = 1
            building["id"] = item["id"].split("/")[1]
            building["house_number"] = props["addr:housenumber"] if "addr:housenumber" in props else ""
            building["street_name"] = props["addr:street"] if "addr:street" in props else ""

            g_type = geometry["type"]
            g_data = ", ".join([" ".join([str(x[1]), str(x[0])]) for x in geometry["coordinates"][0] for y in x])
            building["way"] = g_type.upper() + "((" + g_data + "))"
            # print(building)
            if building["house_number"] and building["street_name"]:
                export_data.append(building)
        except:
            print(traceback.format_exc())
    print("Overall data size from json is: %d" % len(export_data))
    return export_data

def _fields():
    return ["id", "loc_name", "street_name",
        "house_number", "obj_type", "dist_type", "way"]

def _types():
    return OBJECT_TYPE

def connect():
    try:
        con = psycopg2.connect(DB_CREDS)
        con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        return con
    except Exception:
        print(traceback.format_exc())

def db_export(data):
    conn = connect()
    cursor = conn.cursor()
    fields = _fields()
    prep_stmt = "INSERT INTO {table}({keys}) VALUES({values})".format(
                    table = "places",
                    keys = ", ".join(fields),
                    values = ", ".join([("%s" if i != "way" else "ST_GeomFromText(%s, 4326)") for i in fields])
                )
    for building in data:
        values = [(building[field] if field in building else "") for field in fields]
        try:
            cursor.execute(prep_stmt, values)
        except:
            print(traceback.format_exc())

queue = Queue(49)
DATA = {}

def data_thread():
    global queue, DATA
    report = dict()
    try:
        id = queue.get()
        conn = connect()
        print(id)
        query = "SELECT q.loc_name as loc, p.street_name as street, p.house_number as num " + \
            "FROM places as p " + \
            "INNER JOIN (SELECT loc_name, way FROM places WHERE id = %d) as q " + \
            "ON St_DWithin(p.way::geography, ST_Buffer(q.way::geography, 1000), 10) " + \
            "WHERE p.obj_type = 1 ORDER BY street ASC;"
        try:
            print(query)
            cur = conn.cursor()
            cur.execute(query % id)
            res = cur.fetchall()
            report["id"] = id
            report["suburb"] = res[0][0]
            street_dict = dict()
            for rec in res:
                street_name = rec[1]
                house_num = rec[2]
                if not street_name or not house_num: continue
                if street_name in street_dict.keys():
                    house_list = street_dict[street_name]
                    if not house_list: house_list = list()
                    house_list.append(house_num)
                    street_dict[street_name] = house_list
                else:
                    street_dict[street_name] = [house_num]
            report["address"] = street_dict
            DATA[id] = report
        except:
            print(traceback.format_exc())
    except Empty:
        pass

import os

def write_rep():
    if os.path.exists(REPORT_FILE):
        os.remove(REPORT_FILE)
    f = open(REPORT_FILE, "w+")
    for _, building in DATA.items():
        reportString = str()
        reportString += (building["suburb"] + " " + str(building["id"]) + "\n")
        adresses = []
        for street, housenum in building["address"].items:
            pattern = street + "{%s}" % "|".join(housenum)
            adresses.append(pattern)
        reportString += "{%s}" % ("|".join(adresses)) + "\n\n"
        f.write(reportString)

    f.close()

def gen_export():
    global queue, DATA
    suburb_ids = [2136,214,53,67,77,133,44,147,46,98,5,78,121,148,93,146,106,100,25,110,7,65,8,151,88,125,76,145,127,135,96,129,112,132,69,123,83,2133,71,84,216,2143,2142,29]
    # suburb_ids = [2136,214]
    [queue.put(i) for i in suburb_ids]
    queue.task_done()
    threads = list()
    for _id in suburb_ids:
        t = Thread(target=data_thread)
        t.start()
        threads.append(t)

    for t in threads:
        t.join()
    print((DATA))
    write_rep()
    print("Task completed")


if __name__ == '__main__':
    #parse_file()
    gen_export()