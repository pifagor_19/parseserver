﻿DROP TABLE adverts;
DROP TABLE users;
DROP TABLE relations;
DROP SEQUENCE versions;

create sequence versions increment by 1 minvalue 1 no cycle;

CREATE TABLE adverts_lubertsy
(
	guid integer NOT NULL,
	url text NOT NULL,
	category text NOT NULL,
	town text NOT NULL,
	price text,
	phone text,
	contact text,
	address text,
	description text, 
	title text,
	datestamp timestamp,
	CONSTRAINT adverts_lubertsy_pk PRIMARY KEY (guid)
);

CREATE TABLE users
(
	sid serial NOT NULL,
	username text NOT NULL DEFAULT 'guest',
	password text,
	send_interval int DEFAULT 15,
	time_range text[] DEFAULT ARRAY['8.00', '20.00'],
	CONSTRAINT users_pk PRIMARY KEY (sid)
);

CREATE TABLE relations
(
	user_id integer,
	advert_id integer,
	CONSTRAINT relations_unique UNIQUE(user_id, advert_id)
);

CREATE TABLE districts
(
    area_id integer NOT NULL,
    name text,
    description text,
    CONSTRAINT districts_pr_key PRIMARY KEY (area_id)
);

CREATE INDEX ON users(sid);
--CREATE INDEX ON adverts(sid);
CREATE INDEX ON relations(user_id);
CREATE INDEX ON relations(advert_id);

INSERT INTO users(username, password) VALUES('user1', 'password');

--ROLLBACK;

ALTER USER postgres WITH password 'acuario';