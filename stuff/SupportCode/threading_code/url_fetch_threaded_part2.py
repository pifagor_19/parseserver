#!/usr/bin/env python
import Queue
import threading
import urllib2
import time
from BeautifulSoup import BeautifulSoup

hosts = ["http://rambler.ru", "http://google.com", "http://m.mail.ru",
        "http://ya.ru", "http://apple.com"]

queue = Queue.Queue()
out_queue = Queue.Queue()

class ThreadUrl(threading.Thread):
    """Threaded Url Grab"""
    def __init__(self, queue, out_queue):
        threading.Thread.__init__(self)
        self.queue = queue
        self.out_queue = out_queue

    def run(self):
        while True:
            try:
                #grabs host from queue
                host = self.queue.get()

                #grabs urls of hosts and then grabs chunk of webpage
                url = urllib2.urlopen(host)
                chunk = url.read()

                #place chunk into out queue
                self.out_queue.put(chunk)

                #signals to queue job is done
                self.queue.task_done()
            except Queue.Empty:
                pass
            except:
                self.queue.put(host)
            finally:
                time.sleep(2)
                self.queue.task_done()

class DatamineThread(threading.Thread):
    """Threaded Url Grab"""
    def __init__(self, out_queue):
        threading.Thread.__init__(self)
        self.out_queue = out_queue

    def run(self):
        while True:
            try:
                #grabs host from queue
                chunk = self.out_queue.get()

                #parse the chunk
                soup = BeautifulSoup(chunk)
                print soup.findAll(['title'])
                raise Exception
                #signals to queue job is done
            except Queue.Empty:
                pass
            except:
                self.out_queue.put(chunk)
            finally:
                time.sleep(2)
                self.out_queue.task_done()


start = time.time()
def main():

    #spawn a pool of threads, and pass them queue instance
    for i in range(5):
        t = ThreadUrl(queue, out_queue)
        t.setDaemon(True)
        t.start()

    #populate queue with data
    for host in hosts:
        queue.put(host)

    for i in range(5):
        dt = DatamineThread(out_queue)
        dt.setDaemon(True)
        dt.start()


    #wait on the queue until everything has been processed
    queue.join()
    out_queue.join()

main()
print "Elapsed Time: %s" % (time.time() - start)
