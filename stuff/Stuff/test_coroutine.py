# -*- coding: utf-8 -*-
__author__ = 'adm1n'

from Queue import Queue
from threading import Thread

def coroutine(func):
    def start(*args,**kwargs):
        cr = func(*args,**kwargs)
        cr.next()
        return cr
    return start

@coroutine
def threaded(target):
    messages = Queue()
    def run_target():
        while True:
            item = messages.get()
            if item is GeneratorExit:
                target.close()
                return
            else:
                target.send(item)
    Thread(target=run_target).start()
    try:
        while True:
            item = (yield)
            messages.put(item)
    except GeneratorExit:
        messages.put(GeneratorExit)

class Task(object):
    taskid = 0
    def __init__(self,target):
        Task.taskid += 1
        self.tid = Task.taskid # Task ID
        self.target = target # Target coroutine
        self.sendval = None # Value to send
    def run(self):
        return self.target.send(self.sendval)

class Scheduler(object):
    def __init__(self):
        self.ready = Queue()
        self.taskmap = {}

    def new(self,target):
        newtask = Task(target)
        self.taskmap[newtask.tid] = newtask
        self.schedule(newtask)
        return newtask.tid

    def schedule(self,task):
        self.ready.put(task)

    def mainloop(self):
        while self.taskmap:
            task = self.ready.get()
            result = task.run()
            self.schedule(task)

import time

class Test:
    def __init__(self):
        pass

    def foo(self):
        while True:
            try:
                print "I'm foo"
                time.sleep(2)
                yield
                # raise Exception
            except:
                pass

    def bar(self):
        while True:
            try:
                print "I'm bar"
                time.sleep(2)
                yield
                # raise Exception
            except:
                pass

shed = Scheduler()
t = Test()
shed.new(t.foo())
shed.new(t.bar())
shed.mainloop()
